/*INSERT INTO `CONTROLEATENDIMENTO`.`PAIS` (`codPais`, `nomePais`) VALUES ('1', 'Brasil');
*/
INSERT INTO `CONTROLEATENDIMENTO`.`ESTADO` (`codEstado`, `nomeEstado`, `pais_codPais`) VALUES ('1', 'São Paulo', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`CIDADE` (`codCidade`, `nomeCidade`, `estado_codEstado`) VALUES ('1', 'Franco da Rocha', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`BAIRRO` (`codBairro`, `nomeBairro`, `cidade_codCidade`) VALUES ('1', 'Companhia Fazenda Belem', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`RUA` (`codRua`, `cep`, `nomeRua`, `bairro_codBairro`) VALUES ('1', '07803000', 'Rua Doutor Armando Pinto', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`TIPOINSTITUICAO` (`codTipoInstituicao`, `nomeTipoInstituicao`) VALUES ('1', 'CRAS');

INSERT INTO `CONTROLEATENDIMENTO`.`INSTITUICAO` (`codInstituicao`, `codIdInstituicao`, `nomeInstituicao`, `rua_codRua`, `tipoInstituicao_codTipoInstituicao`, `numero`, `telefone`) VALUES ('1', '123', 'Cras Jd Alegria', '1', '1', '52','(11) 4444-6939');

INSERT INTO `CONTROLEATENDIMENTO`.`CARGO` (`codCargo`, `nomeCargo`, `instituicao_codInstituicao`) VALUES ('1', 'TI', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`FUNCIONARIO` (`codFuncionario`, `cpf`, `dataNascimento`, `email`, `estadoCivil`, `nomeCompleto`, `numeroCasa`, `rg`, `sexo`, `situacao`, `telefone`, `cargo_codCargo`, `rua_codRua`, `orgaoEmissor`, `escolaridade`, `coordenador`) VALUES ('1', '370.728.918-46', '1997-09-27', 'milena.silva@franciscomorato.sp.gov.br', 'Solteira', 'Milena Santos da Silva', '169', '39.433.316-0', 'Feminino', b'1', '(11) 97468-9200', '1', '1','SSP-SP', b'1', b'1');

INSERT INTO `CONTROLEATENDIMENTO`.`ROLE` (`nome`) VALUES ('ROLE_ADMIN');

INSERT INTO `CONTROLEATENDIMENTO`.`ROLE` (`nome`) VALUES ('ROLE_COMUM');

/*INSERT INTO `CONTROLEATENDIMENTO`.`USUARIO` (`usuario`, `passwd`, `situacao`, `funcionario_codFuncionario`) VALUES ('milena', '123', b'1', '1');

INSERT INTO `CONTROLEATENDIMENTO`.`USUARIO_ROLE` (`USUARIO_usuario`, `roles_nome`) VALUES ('milena', 'ROLE_ADMIN');*/

INSERT INTO `CONTROLEATENDIMENTO`.`TIPORESPOSTA` ( `nomeTipoResposta`) VALUES ('Texto'), ('Numérico');

INSERT INTO `CONTROLEATENDIMENTO`.`TIPOGRUPO` (`codTipoGrupo`, `nomeTipoGrupo`) VALUES ('1', 'Grupo de Caráter Não Continuado'), ('2', 'Grupo de Caráter Continuado');





