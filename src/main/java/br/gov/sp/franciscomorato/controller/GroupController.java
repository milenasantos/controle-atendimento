/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Grupo;
import br.gov.sp.franciscomorato.model.Registro;
import br.gov.sp.franciscomorato.repository.FuncionarioDAO;
import br.gov.sp.franciscomorato.repository.GrupoDAO;
import br.gov.sp.franciscomorato.repository.RegistroDAO;
import br.gov.sp.franciscomorato.repository.TipoGrupoDAO;
import br.gov.sp.franciscomorato.validators.GrupoValidator;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/group")
public class GroupController {

    @Autowired
    GrupoDAO DAO;
    @Autowired
    TipoGrupoDAO tDAO;
    @Autowired
    RegistroDAO rDAO;
    @Autowired
    FuncionarioDAO fDAO;

    @InitBinder("grupo")
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new GrupoValidator());
    }

    @RequestMapping(value = "{idRegistro}", method = RequestMethod.GET)
    public ModelAndView group(Grupo grupo, @PathVariable("idRegistro") Long cod) {
        grupo.setCodGrupo(DAO.getLastID());
        Registro registro = rDAO.findRegistroById(cod);
        grupo.setRegistro(registro);
        System.out.println("teste----------------------" + grupo.getRegistro());
        ModelAndView view = new ModelAndView("group/group");
        view.addObject("tiposGrupos", tDAO.listAll());
        view.addObject("funcs", fDAO.findAll());
        return view;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView groupTest(Grupo grupo, Long cod) {
        grupo.setCodGrupo(DAO.getLastID());
        Registro registro = rDAO.findRegistroById(cod);
        grupo.setRegistro(registro);
        System.out.println("teste----------------------" + grupo.getRegistro());
        ModelAndView view = new ModelAndView("group/group");
        view.addObject("tiposGrupos", tDAO.listAll());
        view.addObject("funcs", fDAO.findAll());
        return view;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET})
    @Transactional
    public ModelAndView save(@Valid Grupo grupo, Long pCodRegistro,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {

            return groupTest(grupo, pCodRegistro);
        } else {
            grupo.setCodGrupo(null);
            
            Registro registro = rDAO.findRegistroById(pCodRegistro);
            grupo.setRegistro(registro);
            DAO.save(grupo);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/group");
        }
    }
}
