/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Pergunta;
import br.gov.sp.franciscomorato.model.TipoResposta;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.ModalidadeDAO;
import br.gov.sp.franciscomorato.repository.PerguntaDAO;
import br.gov.sp.franciscomorato.repository.TipoRespostaDAO;
import br.gov.sp.franciscomorato.validators.PerguntaValidator;
import com.google.gson.Gson;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    PerguntaDAO DAO;
    @Autowired
    ModalidadeDAO mDAO;
    @Autowired
    TipoRespostaDAO tDAO;

    @InitBinder
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new PerguntaValidator());
        // com essa sobrescrita do Collection o Spring consegue popular a list
        // de roles direto pelo binding
        CustomCollectionEditor tipoRespostaCollector = new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                if (element instanceof String) {
                    TipoResposta tipoResposta = new TipoResposta();
                    tipoResposta.setNomeTipoResposta((String) element);
                    return tipoResposta;
                }
                throw new RuntimeException("Spring says: Não sei o que fazer com esse elemento: " + element);
            }
        };

        webDataBinder.registerCustomEditor(List.class, "tipoResposta", tipoRespostaCollector);
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView view = new ModelAndView("question/listar");
        view.addObject("lista", new Gson().toJson(DAO.findPerguntabyModalidade()));
        view.addObject("modalidade", mDAO.listAll());
        return view;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView question(Pergunta pergunta) {
        ModelAndView view = new ModelAndView("question/question");
        pergunta.setCodPergunta(DAO.getLastID());
        view.addObject("modalidades", mDAO.listAll());
        view.addObject("tiposRespostas", tDAO.listAll());
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    //@CacheEvict(value = "perguntasRecentes", allEntries = true)
    public ModelAndView save(@Valid Pergunta pergunta, BindingResult bindingResult,
            RedirectAttributes redirectAttributes/*, String roles, String funcionario*/) {
        ModelAndView view = new ModelAndView("redirect:/question");
        // Se tiver erros no form (como campos nulos (ver validações em UsuarioValidator))
        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            for (FieldError error : errors) {
                System.out.println(error.getObjectName() + " -123 " + error.getDefaultMessage());
            }
            return question(pergunta);
        } else {

            DAO.save(pergunta);
            redirectAttributes.addFlashAttribute("success", "true");
            return view;
        }
    }
}
