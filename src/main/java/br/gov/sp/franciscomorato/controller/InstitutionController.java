/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Instituicao;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.EnderecoDAO;
import br.gov.sp.franciscomorato.repository.InstituicaoDAO;
import br.gov.sp.franciscomorato.repository.ModalidadeDAO;
import br.gov.sp.franciscomorato.repository.PerguntaDAO;
import br.gov.sp.franciscomorato.repository.TipoInstituicaoDAO;
import br.gov.sp.franciscomorato.validators.InstituicaoValidator;
import com.google.gson.Gson;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/institution")
public class InstitutionController {

    @Autowired
    InstituicaoDAO DAO;
    @Autowired
    TipoInstituicaoDAO tDAO;
    @Autowired
    EnderecoDAO eDAO;
    @Autowired
    ModalidadeDAO mDAO;
    @Autowired
    PerguntaDAO pDAO;

    @InitBinder("instituicao")
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new InstituicaoValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView institution(Instituicao instituicao) {
        instituicao.setCodInstituicao(DAO.getLastID());
        ModelAndView view = new ModelAndView("institution/institution");
        view.addObject("tiposInstituicoes", tDAO.listAll());
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    //@CacheEvict(value={}, allEntries=true)
    public ModelAndView save(@Valid Instituicao instituicao, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return institution(instituicao);
        } else {
            instituicao.setCodInstituicao(null);

            instituicao.setRua(eDAO.criaEndereco(instituicao.getRua()));
            DAO.save(instituicao);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/institution");
        }
    }

    @RequestMapping("list")
    //@Cacheable(value = "tiposInstituicoesRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("institution/listar");

        view.addObject("lista", new Gson().toJson(DAO.findInstituicao()));
        return view;
    }

    @RequestMapping("{id}")
    public ModelAndView institution(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("institution/show");
        Instituicao i = DAO.findInstituicaoByID(id);
        if (i.getCodInstituicao() != null) {
            view.addObject("instituicao", i);
            view.addObject("tiposInstituicoes", tDAO.listAll());
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "update")
    @Transactional
    @CacheEvict(value = {"funcionariosRecentes"}, allEntries = true)
    public ModelAndView update(@Valid Instituicao instituicao,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return new ModelAndView("institution/show").addObject("institution", instituicao).addObject("tiposInstituicoes", tDAO.listAll());
        } else {
            instituicao.setRua(eDAO.criaEndereco(instituicao.getRua()));
            DAO.save(instituicao);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/institution/" + instituicao.getCodInstituicao());
        }
    }
    
    /**MÉTODO SENDO CHAMADO NA LISTA DE INSTITUIÇÕES**/
    /*@RequestMapping("{id}")
    public ModelAndView record(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("record/record");
        Instituicao i = DAO.findInstituicaoByID(id);
        List<Modalidade> m = mDAO.findModalidadeByTipoInstituicao(i.getTipoInstituicao().getCodTipoInstituicao());
        if (i.getCodInstituicao() != null) {
            view.addObject("instituicao", i);
            view.addObject("tipoinstituicao", tDAO.listAll());
            //view.addObject("modalidade", mDAO.findModalidadeByTipoInstituicao(
                    //i.getTipoInstituicao().getCodTipoInstituicao()));
            //for(int j=0; j<m.size();j++){
                //System.out.println(m.get(j).getCodModalidade());
                view.addObject("perguntas", pDAO.findPerguntaByTipoInstituicao(i.getTipoInstituicao().getCodTipoInstituicao()));
                System.out.println(pDAO.findPerguntaByTipoInstituicao(i.getTipoInstituicao().getCodTipoInstituicao()));
            //}
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }*/
}
