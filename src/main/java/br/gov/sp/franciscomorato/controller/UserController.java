/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Role;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.FuncionarioDAO;
import br.gov.sp.franciscomorato.repository.RoleDAO;
import br.gov.sp.franciscomorato.repository.UsuarioDAO;
import br.gov.sp.franciscomorato.validators.UsuarioValidator;
import com.google.gson.Gson;
import java.util.List;
import java.util.Objects;
import javax.el.PropertyNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    FuncionarioDAO fDAO;
    @Autowired
    RoleDAO rDAO;

    /*
  * Metodo para adicionar validadores para este controller
     */
    @InitBinder
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new UsuarioValidator());
        // com essa sobrescrita do Collection o Spring consegue popular a list
        // de roles direto pelo binding
        CustomCollectionEditor rolesCollector = new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                if (element instanceof String) {
                    Role role = new Role();
                    role.setNome((String) element);
                    return role;
                }
                throw new RuntimeException("Spring says: Não sei o que fazer com esse elemento: " + element);
            }
        };

        webDataBinder.registerCustomEditor(List.class, "roles", rolesCollector);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView user(Usuario usuario) {
        ModelAndView view = new ModelAndView("user/user");
        view.addObject("codigo", usuarioDAO.getLastID() + 1);
        view.addObject("funcionarios", fDAO.findAll());
        view.addObject("regras", rDAO.listAll());
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    @CacheEvict(value = "usuariosRecentes", allEntries = true)
    public ModelAndView save(@Valid Usuario usuario, BindingResult bindingResult,
            RedirectAttributes redirectAttributes/*, String roles, String funcionario*/) {
        ModelAndView view = new ModelAndView("redirect:/user");
        // Se tiver erros no form (como campos nulos (ver validações em UsuarioValidator))
        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            for (FieldError error : errors) {
                System.out.println(error.getObjectName() + " - " + error.getDefaultMessage());
            }
            return user(usuario);
        } else {
            usuario.setFuncionario(fDAO.findFuncionarioByID(usuario.getFuncionario().getCodFuncionario()));
            usuario.setPasswd(new BCryptPasswordEncoder().encode(usuario.getPasswd()));
            usuarioDAO.save(usuario);
            redirectAttributes.addFlashAttribute("success", "true");
            return view;
        }
    }

    @RequestMapping("list")
    //@Cacheable(value = "cargosRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("user/listar");
        view.addObject("lista", new Gson().toJson(usuarioDAO.findUsers()));
        return view;
    }

    @RequestMapping(value = "{user}", method = RequestMethod.GET)
    @Transactional
    public ModelAndView user(@PathVariable("user") String user) throws PropertyNotFoundException{
        ModelAndView view = new ModelAndView("/user/show");
        Usuario u = usuarioDAO.findUserById(user);
        if (!Objects.isNull(u) && u.getUsuario() != null) {
            view.addObject("usuario", u);
            view.addObject("funcionarios", fDAO.findAll());
            view.addObject("regras", rDAO.listAll());
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }
}
