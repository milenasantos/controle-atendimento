
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Funcionario;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.CargoDAO;
import br.gov.sp.franciscomorato.repository.EnderecoDAO;
import br.gov.sp.franciscomorato.repository.FuncionarioDAO;
import br.gov.sp.franciscomorato.validators.FuncionarioValidator;
import com.google.gson.Gson;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    FuncionarioDAO DAO;
    @Autowired
    CargoDAO cDAO;
    @Autowired
    EnderecoDAO eDAO;

    @InitBinder
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new FuncionarioValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView employee(Funcionario funcionario) {
        funcionario.setCodFuncionario(DAO.getLastID());
        ModelAndView view = new ModelAndView("employee/employee");
        view.addObject("cargos", cDAO.listAll());
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    //@CacheEvict(value = {"funcionariosRecentes"}, allEntries = true)
    public ModelAndView save(@Valid Funcionario funcionario, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return employee(funcionario);
        } else {
            funcionario.setCodFuncionario(null);

            funcionario.setRua(eDAO.criaEndereco(funcionario.getRua()));
            DAO.save(funcionario);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/employee");
        }
    }

    @RequestMapping("list")
    @Cacheable(value = "funcionariosRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("employee/listar");
        view.addObject("lista", new Gson().toJson(DAO.findFuncionario()));
        return view;
    }

    @RequestMapping(method = RequestMethod.POST, value = "update")
    @Transactional
    @CacheEvict(value = {"funcionariosRecentes"}, allEntries = true)
    public ModelAndView update(@Valid Funcionario funcionario,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return new ModelAndView("employee/show").addObject("funcionario", funcionario).addObject("cargos", cDAO.listAll());
        } else {
            funcionario.setRua(eDAO.criaEndereco(funcionario.getRua()));
            DAO.save(funcionario);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/employee/" + funcionario.getCodFuncionario());
        }
    }

    @RequestMapping("{id}")
    public ModelAndView employee(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("employee/show");
        Funcionario f = DAO.findFuncionarioByID(id);
        if (f.getCodFuncionario() != null) {
            view.addObject("funcionario", f);
            view.addObject("cargos", cDAO.listAll());
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }
}