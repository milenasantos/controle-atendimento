/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Modalidade;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.ModalidadeDAO;
import br.gov.sp.franciscomorato.repository.TipoInstituicaoDAO;
import br.gov.sp.franciscomorato.validators.ModalidadeValidator;
import com.google.gson.Gson;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/modality")
public class ModalityController {

    @Autowired
    ModalidadeDAO DAO;
    @Autowired
    TipoInstituicaoDAO tDAO;

    @InitBinder("modalidade")
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new ModalidadeValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView modality(Modalidade modalidade) {
        modalidade.setCodModalidade(DAO.getLastID());
        ModelAndView view = new ModelAndView("modality/modality");
        view.addObject("tiposInstituicoes", tDAO.listAll());
        view.addObject("lista", new Gson().toJson(DAO.findModalidade()));
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    //@CacheEvict(value={}, allEntries=true)
    public ModelAndView save(@Valid Modalidade modalidade, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return modality(modalidade);
        } else {
            modalidade.setCodModalidade(null);

            DAO.save(modalidade);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/modality");
        }
    }

    @RequestMapping("list")
    @Cacheable(value = "modalidadesRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("modality/listar");

        view.addObject("lista", new Gson().toJson(DAO.findModalidade()));
        return view;
    }
    
    @RequestMapping("{id}")
    public ModelAndView institution(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("modality/show");
        Modalidade m = DAO.findModalidadeByID(id);
        if (m.getCodModalidade()!= null) {
            view.addObject("modalidade", m);
            view.addObject("tiposInstituicoes", tDAO.listAll());
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "update")
    @Transactional
    @CacheEvict(value = {"modalidadesRecentes"}, allEntries = true)
    public ModelAndView update(@Valid Modalidade modalidade,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return new ModelAndView("modality/show").addObject("modality", modalidade).addObject("tiposInstituicoes", tDAO.listAll());
        } else {
            DAO.save(modalidade);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/modality/" + modalidade.getCodModalidade());
        }
    }
}
