/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Instituicao;
import br.gov.sp.franciscomorato.model.TipoInstituicao;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.TipoInstituicaoDAO;
import com.google.gson.Gson;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/institutionType")
public class InstitutionTypeController {

    @Autowired
    private TipoInstituicaoDAO DAO;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView institutionType(TipoInstituicao tipoInstituicao) {
        tipoInstituicao.setCodTipoInstituicao(DAO.getLastID());
        ModelAndView view = new ModelAndView("institutionType/institutionType");
        view.addObject("lista", new Gson().toJson(DAO.findTipoInstituicao()));
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid TipoInstituicao tipoInstituicao,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return institutionType(tipoInstituicao);
        } else {
            tipoInstituicao.setCodTipoInstituicao(null);
            DAO.save(tipoInstituicao);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/institutionType");
        }
    }
    
    @RequestMapping("list")
    //@Cacheable(value = "tiposInstituicoesRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("institutionType/listar");

        view.addObject("lista", new Gson().toJson(DAO.findTipoInstituicao()));
        return view;
    }
    
    @RequestMapping("{id}")
    public ModelAndView institutionType(@PathVariable("id") Long id) {
        ModelAndView view = new ModelAndView("institutionType/show");
        TipoInstituicao t = DAO.findTipoInstituicaoByID(id);
        if (t.getCodTipoInstituicao()!= null) {
            view.addObject("tipoInstituicao", t);
        } else {
            view.setViewName("errors/404");
        }
        return view;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "update")
    @Transactional
    @CacheEvict(value = {"funcionariosRecentes"}, allEntries = true)
    public ModelAndView update(@Valid TipoInstituicao tipoInstituicao,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return new ModelAndView("institutionType/show").addObject("institutionType", tipoInstituicao);
        } else {
            DAO.save(tipoInstituicao);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/institutionType/" + tipoInstituicao.getCodTipoInstituicao());
        }
    }
}
