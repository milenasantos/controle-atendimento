package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.UsuarioDAO;
import br.gov.sp.franciscomorato.validators.UsuarioValidator;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Jhonatan Colina
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @InitBinder
    public void addValidators(WebDataBinder webDataBinder) {        
        
        webDataBinder.addValidators(new UsuarioValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(Usuario login, boolean falhaLogin) {
        ModelAndView view = new ModelAndView("index");
        view.addObject("falhaLogin", falhaLogin);
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ModelAndView doLogin(@Valid Usuario login, BindingResult bindingResult,
            RedirectAttributes redirectAttributes, HttpServletResponse response) {
        ModelAndView view = new ModelAndView("redirect:home");
       
        if (bindingResult.hasErrors()) {
            return index(login, false);
        }
       
        if (login != null) {
            System.out.println(login.toString());
            response.addCookie(new Cookie("usuarioLogado", login.getUsername()));
            return view;
        } else {
            return index(login, true);
        }
    }
}