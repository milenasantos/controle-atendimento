/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Cargo;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.CargoDAO;
import br.gov.sp.franciscomorato.repository.InstituicaoDAO;
import br.gov.sp.franciscomorato.repository.TipoInstituicaoDAO;
import br.gov.sp.franciscomorato.validators.CargoValidator;
import com.google.gson.Gson;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/occupation")
public class OccupationController {

    @Autowired
    CargoDAO DAO;
    @Autowired
    TipoInstituicaoDAO tDAO;
    @Autowired
    InstituicaoDAO iDAO;

    @InitBinder
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new CargoValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    @Transactional
    public ModelAndView occupation(Cargo cargo) {
        cargo.setCodCargo(DAO.getLastID());
        ModelAndView view = new ModelAndView("occupation/occupation");
        view.addObject("istituicoes", iDAO.listAll());
        view.addObject("lista", new Gson().toJson(DAO.findCargo()));
        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid Cargo cargo,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return occupation(cargo);
        } else {
            cargo.setCodCargo(null);
            DAO.save(cargo);
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/occupation");
        }
    }

    @RequestMapping("list")
    //@Cacheable(value = "cargosRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("occupation/listar");
        view.addObject("lista", new Gson().toJson(DAO.findCargo()));
        return view;
    }
}
