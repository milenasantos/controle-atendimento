/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.controller;

import br.gov.sp.franciscomorato.model.Instituicao;
import br.gov.sp.franciscomorato.model.Pergunta;
import br.gov.sp.franciscomorato.model.Registro;
import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.InstituicaoDAO;
import br.gov.sp.franciscomorato.repository.ModalidadeDAO;
import br.gov.sp.franciscomorato.repository.PerguntaDAO;
import br.gov.sp.franciscomorato.repository.RegistroDAO;
import br.gov.sp.franciscomorato.repository.RespostaDAO;
import br.gov.sp.franciscomorato.repository.TipoGrupoDAO;
import br.gov.sp.franciscomorato.validators.RegistroValidador;
import com.google.gson.Gson;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author milena
 */
@Controller
@RequestMapping("/record")
public class RecordController {

    @Autowired
    RegistroDAO DAO;
    @Autowired
    PerguntaDAO pDAO;
    @Autowired
    InstituicaoDAO iDAO;
    @Autowired
    RespostaDAO rDAO;
    @Autowired
    TipoGrupoDAO tDAO;
    @Autowired
    ModalidadeDAO mDAO;
 
    @InitBinder("registro")
    public void addValidators(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new RegistroValidador());
    }

    @RequestMapping("{id}")
    @Transactional
    public ModelAndView recordById(Registro registro, @PathVariable("id") Long id) {
        Instituicao i = iDAO.findInstituicaoByID(id);

        registro.setCodRegistro(DAO.getLastID());
        registro.setInstituicao(i);
        ModelAndView view = new ModelAndView("record/record");

        //view.addObject("modalidades", mDAO.findModalidadeByTipoInstituicao(i.getTipoInstituicao().getCodTipoInstituicao()));
        view.addObject("perguntas", pDAO.findPerguntasByTipoInstituicaoTeste(i.getTipoInstituicao().getCodTipoInstituicao()));
        //view.addObject("tiposGrupos", tDAO.listAll());

        return view;
    }

    /*@RequestMapping(method = RequestMethod.GET)
    @Transactional
    public ModelAndView record(Registro registro,
            @AuthenticationPrincipal Usuario user) {

        Long codTipo = user.getFuncionario().getCargo().getInstituicao().getTipoInstituicao().getCodTipoInstituicao();
        Instituicao i = iDAO.findInstituicaoByID(user.getFuncionario().getCargo().getInstituicao().getCodInstituicao());

        registro.setCodRegistro(DAO.getLastID());
        registro.setInstituicao(i);
        ModelAndView view = new ModelAndView("record/record");

        //view.addObject("modalidades", mDAO.findModalidadeByTipoInstituicaoTeste(i.getTipoInstituicao().getCodTipoInstituicao()));
        view.addObject("perguntas", pDAO.findPerguntaByTipoInstituicao(codTipo));
        view.addObject("tiposGrupos", tDAO.listAll());
        return view;
    }*/

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    //@CacheEvict(value={}, allEntries=true)
    public ModelAndView save(@Valid Registro registro, BindingResult bindingResult,
            RedirectAttributes redirectAttributes, String resp, String pCodModalidade,
            @AuthenticationPrincipal Usuario user) {

        if (bindingResult.hasErrors()) {
            //return record(registro, user);
            return null;
        } else {
            registro.setCodRegistro(null);

            DAO.save(registro);

            Long codTipo = user.getFuncionario().getCargo().getInstituicao().getTipoInstituicao().getCodTipoInstituicao();
            List<Pergunta> pergunta = pDAO.findPerguntaByTipoInstituicao(codTipo);

            System.out.println("************************************************" + resp);

            /*for (int i = 0; i < pergunta.size(); i++) {
                Resposta resposta = new Resposta(resp, pergunta.get(i), DAO.retunrLastID());
                rDAO.save(resposta);
            }*/
            redirectAttributes.addFlashAttribute("success", "true");
            return new ModelAndView("redirect:/record");
        }
    }

    @RequestMapping("list")
    //@Cacheable(value = "tiposInstituicoesRecentes") // coloca como cache de memoria
    public ModelAndView list(@AuthenticationPrincipal Usuario user) {
        ModelAndView view = new ModelAndView("record/listar");

        Long pCod = user.getFuncionario().getCargo().getInstituicao().getCodInstituicao();

        view.addObject("lista", new Gson().toJson(DAO.findRegistroByInstituicao(pCod)));
        return view;
    }

}
