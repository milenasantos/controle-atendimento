/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Registro;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class RegistroDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Registro r) {
        em.merge(r);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select r from REGISTRO r order by r.codRegistro desc", Registro.class).setMaxResults(1).getSingleResult().getCodRegistro() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public Registro retunrLastID() {
        try {
            return em.createQuery("select r from REGISTRO r order by r.codRegistro desc", Registro.class).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Registro> findRegistroByInstituicao(Long pCodInstituicao) {
        List<Registro> registros = new ArrayList<>();
        try {
            registros = em.createQuery("select r from REGISTRO r join FETCH r.instituicao i where i.codInstituicao=:codInstituicao", Registro.class)
                    .setParameter("codInstituicao", pCodInstituicao)
                    .getResultList();
        } catch (NoResultException ex) {
        }

        return registros;
    }

    public Registro findRegistroById(Long pCodRegistro) {
        Registro r = new Registro();
        try {
            r = em.createQuery("select r from REGISTRO r where r.codRegistro=:codRegistro", Registro.class)
                    .setParameter("codRegistro", pCodRegistro)
                    .getSingleResult();
        } catch (NoResultException ex) {

        }

        return r;
    }
}
