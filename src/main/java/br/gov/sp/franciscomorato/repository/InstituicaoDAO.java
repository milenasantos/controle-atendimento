/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Instituicao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class InstituicaoDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Instituicao i) {
        em.merge(i);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select i from INSTITUICAO i order by i.codInstituicao desc", Instituicao.class).setMaxResults(1).getSingleResult().getCodInstituicao() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<Instituicao> listAll() {
        List<Instituicao> it = em.createQuery("select i from INSTITUICAO i", Instituicao.class).getResultList();
        return it;
    }

    public List<Instituicao> findInstituicao() {
        List<Instituicao> instituicoes = new ArrayList<>();
        try {
            instituicoes = em.createQuery("select i from INSTITUICAO i", Instituicao.class).getResultList();
        } catch (NoResultException ex) {
        }

        return instituicoes;
    }

    public Instituicao findInstituicaoByID(Long pCodInstituicao) {
        Instituicao i = new Instituicao();
        try {
            i = em.createQuery("select i from INSTITUICAO i join FETCH i.tipoInstituicao t where i.codInstituicao=:codInstituicao", Instituicao.class)
                    .setParameter("codInstituicao", pCodInstituicao)
                    .getSingleResult();
        } catch (NoResultException ex) {
        }
        return i;
    }
}
