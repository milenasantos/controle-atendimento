/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Funcionario;
import br.gov.sp.franciscomorato.model.TipoInstituicao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class TipoInstituicaoDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(TipoInstituicao t) {
        em.merge(t);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select t from TIPOINSTITUICAO t order by t.codTipoInstituicao desc", TipoInstituicao.class).setMaxResults(1).getSingleResult().getCodTipoInstituicao() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<TipoInstituicao> listAll() {
        List<TipoInstituicao> ti = em.createQuery("select t from TIPOINSTITUICAO t", TipoInstituicao.class).getResultList();
        return ti;
    }
    
     public List<TipoInstituicao> findTipoInstituicao() {
        List<TipoInstituicao> tiposInstituicoes = new ArrayList<>();
        try {
            tiposInstituicoes = em.createQuery("select t from TIPOINSTITUICAO t", TipoInstituicao.class).getResultList();
        } catch (NoResultException ex) {
        }

        return tiposInstituicoes;
    }
     
     public TipoInstituicao findTipoInstituicaoByID(Long pCodTipoInstituicao) {
        TipoInstituicao t = new TipoInstituicao();
        try {
            t = em.createQuery("select t from TIPOINSTITUICAO t where t.codTipoInstituicao=:codTipoInstituicao", TipoInstituicao.class)
                    .setParameter("codTipoInstituicao", pCodTipoInstituicao)
                    .getSingleResult();
        } catch (NoResultException ex) {
        }
        return t;
    }
}
