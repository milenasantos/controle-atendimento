/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Grupo;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class GrupoDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Grupo g) {
        em.merge(g);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select g from GRUPO g order by g.codGrupo desc", Grupo.class).setMaxResults(1).getSingleResult().getCodGrupo() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }
}
