/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Bairro;
import br.gov.sp.franciscomorato.model.Cidade;
import br.gov.sp.franciscomorato.model.Estado;
import br.gov.sp.franciscomorato.model.Pais;
import br.gov.sp.franciscomorato.model.Rua;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class EnderecoDAO {

    @PersistenceContext
    private EntityManager em;

    public Rua criaEndereco(Rua obj) {
        obj.getBairro().getCidade().getEstado().setPais(findPais(obj.getBairro().getCidade().getEstado().getPais()));
        obj.getBairro().getCidade().setEstado(findEstado(obj.getBairro().getCidade().getEstado()));
        obj.getBairro().setCidade(findCidade(obj.getBairro().getCidade()));
        obj.setBairro(findBairro(obj.getBairro()));
        obj = findRua(obj);
        return obj;
    }

    public Rua findRua(Rua r) {
        try {
            return em.createQuery("select r from RUA r where r.nomeRua=:rua and "
                    + "r.bairro.nomeBairro=:bairro and "
                    + "r.bairro.cidade.nomeCidade=:cidade and "
                    + "r.bairro.cidade.estado.nomeEstado=:estado and "
                    + "r.bairro.cidade.estado.pais.nomePais=:pais", Rua.class)
                    .setParameter("rua", r.getNomeRua())
                    .setParameter("bairro", r.getBairro().getNomeBairro())
                    .setParameter("cidade", r.getBairro().getCidade().getNomeCidade())
                    .setParameter("estado", r.getBairro().getCidade().getEstado().getNomeEstado())
                    .setParameter("pais", r.getBairro().getCidade().getEstado().getPais().getNomePais())
                    .getSingleResult();
        } catch (NoResultException ex) {
            return r;
        }
    }

    public Bairro findBairro(Bairro b) {
        try {
            return em.createQuery("select b from BAIRRO b where b.nomeBairro=:bairro and "
                    + "b.cidade.nomeCidade=:cidade and "
                    + "b.cidade.estado.nomeEstado=:estado and "
                    + "b.cidade.estado.pais.nomePais=:pais", Bairro.class)
                    .setParameter("bairro", b.getNomeBairro())
                    .setParameter("cidade", b.getCidade().getNomeCidade())
                    .setParameter("estado", b.getCidade().getEstado().getNomeEstado())
                    .setParameter("pais", b.getCidade().getEstado().getPais().getNomePais())
                    .getSingleResult();
        } catch (NoResultException ex) {
            return b;
        }
    }

    public Cidade findCidade(Cidade c) {
        try {
            return em.createQuery("select c from CIDADE c where c.nomeCidade=:cidade and "
                    + "c.estado.nomeEstado=:estado and "
                    + "c.estado.pais.nomePais=:pais", Cidade.class)
                    .setParameter("cidade", c.getNomeCidade())
                    .setParameter("estado", c.getEstado().getNomeEstado())
                    .setParameter("pais", c.getEstado().getPais().getNomePais())
                    .getSingleResult();

        } catch (NoResultException ex) {
            return c;
        }
    }

    public Estado findEstado(Estado e) {
        try {
            return em.createQuery("select e from ESTADO e where e.nomeEstado=:estado and e.pais.nomePais=:pais ", Estado.class)
                    .setParameter("estado", e.getNomeEstado())
                    .setParameter("pais", e.getPais().getNomePais())
                    .getSingleResult();
        } catch (NoResultException ex) {
            return e;
        }
    }

    public Pais findPais(Pais p) {
        try {
            return em.createQuery("select p from PAIS p where p.nomePais=:pais", Pais.class)
                    .setParameter("pais", p.getNomePais())
                    .getSingleResult();
        } catch (NoResultException ez) {
            return p;
        }
    }
}
