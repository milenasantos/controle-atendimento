/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Funcionario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class FuncionarioDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Funcionario f) {
        em.merge(f);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select f from FUNCIONARIO f order by f.codFuncionario desc", Funcionario.class).setMaxResults(1).getSingleResult().getCodFuncionario() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<Funcionario> findAll() {
        //return em.createQuery("select f from FUNCIONARIO f join FETCH f.departamento d join FETCH f.rua r where f.situacao != 0",Funcionario.class)
        return em.createQuery("select f from FUNCIONARIO f where f.situacao != 0", Funcionario.class)
                .getResultList();
    }

    public List<Funcionario> findFuncionario() {
        List<Funcionario> funcionarios = new ArrayList<>();
        try {
            funcionarios = em.createQuery("select f from FUNCIONARIO f join FETCH f.cargo c join FETCH f.rua r", Funcionario.class).getResultList();
        } catch (NoResultException ex) {
        }
        return funcionarios;
    }

    public Funcionario findFuncionarioByID(Long pCodFuncionario) {
        Funcionario f = new Funcionario();
        try {
            f = em.createQuery("select f from FUNCIONARIO f join FETCH f.cargo c join FETCH f.rua r where f.codFuncionario=:codFuncionario", Funcionario.class)
                    .setParameter("codFuncionario", pCodFuncionario)
                    .getSingleResult();
        } catch (NoResultException ex) {
        }
        return f;
    }
}
