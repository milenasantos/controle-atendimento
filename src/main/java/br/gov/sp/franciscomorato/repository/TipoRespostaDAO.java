/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.TipoResposta;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class TipoRespostaDAO {

    @PersistenceContext
    private EntityManager em;

    /*public List<TipoResposta> listAll() {
        List<TipoResposta> tr = em.createQuery("select t from TIPORESPOSTA t", TipoResposta.class).getResultList();
        return tr;
    }*/
    public List<TipoResposta> listAll() {
        return em.createQuery("select t from TIPORESPOSTA t", TipoResposta.class)
                .getResultList();
    }
}
