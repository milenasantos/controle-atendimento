/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.TipoGrupo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class TipoGrupoDAO {
    @PersistenceContext
    private EntityManager em;
    
    public List<TipoGrupo> listAll() {
        List<TipoGrupo> tg = em.createQuery("select t from TIPOGRUPO t", TipoGrupo.class).getResultList();
        return tg;
    }
}
