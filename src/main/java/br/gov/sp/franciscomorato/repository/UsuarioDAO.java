/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Cargo;
import br.gov.sp.franciscomorato.model.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class UsuarioDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Usuario u) {
        em.merge(u);
    }

    public Long getLastID() {
        try {
            return (Long) em.createQuery("select count(u) from USUARIO u").getSingleResult();
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public Usuario findUser(String usuario) {
        try {
            return em.createQuery("select u from USUARIO u where u.usuario=:user", Usuario.class)
                    .setParameter("user", usuario)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Usuario> findUsers() {
        List<Usuario> users = new ArrayList<>();
        try {
            users = em.createQuery("select u from USUARIO u", Usuario.class).getResultList();
        } catch (NoResultException ex) {
        }

        return users;
    }

    public Usuario findUserById(String usuario) {
        try {
            return em.createQuery("select u from USUARIO u join FETCH u.funcionario f join FETCH f.cargo c where u.usuario = :user", Usuario.class)
                    .setParameter("user", usuario)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
