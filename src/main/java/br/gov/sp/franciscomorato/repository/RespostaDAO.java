/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Resposta;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class RespostaDAO {
    @PersistenceContext
    private EntityManager em;
    
    public void save(Resposta r){
        em.merge(r);
    }
    
    public Long getLastID() {
        try {
            return em.createQuery("select r from RESPOSTA r order by r.codResposta desc", Resposta.class).setMaxResults(1).getSingleResult().getCodResposta()+ 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }
}
