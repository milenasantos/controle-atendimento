package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Role;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class RoleDAO
{
  @PersistenceContext
  private EntityManager em;
  
  public List<Role> listAll()
  {
    return em.createQuery("select r from ROLE r",Role.class)
            .getResultList();
  }
}
