/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Modalidade;
import br.gov.sp.franciscomorato.model.Pergunta;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class PerguntaDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Pergunta p) {
        em.merge(p);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select p from PERGUNTA p order by p.codPergunta desc", Pergunta.class).setMaxResults(1).getSingleResult().getCodPergunta() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<Pergunta> listAll() {
        List<Pergunta> pt = em.createQuery("select p from PERGUNTA p", Pergunta.class).getResultList();
        return pt;
    }
    
    public List<Pergunta> findPerguntabyModalidade() {
        List<Pergunta> perguntas = new ArrayList<>();
        try {
            perguntas = em.createQuery("select p from PERGUNTA p", Pergunta.class).getResultList();
        } catch (NoResultException ex) {
            System.out.println("********************************* NO RESULT");
        }

        return perguntas;
    }

    public List<Pergunta> findPerguntaByTipoInstituicao(Long pCodTipo) {
        List<Pergunta> perguntas = new ArrayList<>();
        try {
            perguntas = em.createQuery("select p from PERGUNTA p join FETCH p.modalidade m join m.tipoInstituicao t where t.codTipoInstituicao=:codTipoInstituicao", Pergunta.class)
                    .setParameter("codTipoInstituicao", pCodTipo)
                    .getResultList();
        } catch (NoResultException ex) {
        }

        return perguntas;
    }

    public List<Pergunta> findPerguntasByTipoInstituicaoTeste(Long pCodTipo) {
        List<Pergunta> perguntas = new ArrayList<>();

        try {
            /*String sql = "select m.nomeModalidade as nome, group_concat(p.pergunta) as pergunta, t.nomeTipoInstituicao as tipoInstituicao "
                    + "from MODALIDADE m "
                    + "inner join PERGUNTA p on m.codModalidade=p.modalidade_codModalidade "
                    + "inner join TIPOINSTITUICAO t on m.tipoInstituicao.codTipoInstituicao=t.codTipoInstituicao "
                    + "where t.codTipoInstituicao =:codTipoInstituicao "
                    + "group by m.codModalidade ";
            modalidades = (List<Modalidade>) em.createQuery(sql, Modalidade.class).
                    setParameter("codTipoInstituicao", pCodTipo);*/

            Query query = em.createNativeQuery("select p.pergunta, m.nomeModalidade "
                    + "from MODALIDADE m "
                    + "inner join PERGUNTA p on m.codModalidade=p.modalidade_codModalidade "
                    + "where m.tipoInstituicao_codTipoInstituicao = 1 "
                    + "group by p.codPergunta ");
           
            List<Object[]> list = query.getResultList();
            for (Object[] l : list) {
                Pergunta p = new Pergunta(l[0].toString(), l[1].toString());
                
                perguntas.add(p);
                
            }
        } catch (NoResultException e) {
        }
        return perguntas;
    }
}
