/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Cargo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class CargoDAO {

    @PersistenceContext
    private EntityManager em;
    
    public void save(Cargo c) {
        em.merge(c);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select c from CARGO c order by c.codCargo desc", Cargo.class).setMaxResults(1).getSingleResult().getCodCargo() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<Cargo> listAll() {
        List<Cargo> cg = em.createQuery("select c from CARGO c", Cargo.class).getResultList();
        return cg;
    }
    
     public List<Cargo> findCargo() {
        List<Cargo> cargos = new ArrayList<>();
        try {
            cargos = em.createQuery("select c from CARGO c", Cargo.class).getResultList();
        } catch (NoResultException ex) {
        }

        return cargos;
    }
}
