/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.repository;

import br.gov.sp.franciscomorato.model.Modalidade;
import br.gov.sp.franciscomorato.model.Pergunta;
import br.gov.sp.franciscomorato.model.TipoInstituicao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author milena
 */
@Repository
public class ModalidadeDAO {

    @PersistenceContext
    private EntityManager em;

    public void save(Modalidade m) {
        em.merge(m);
    }

    public Long getLastID() {
        try {
            return em.createQuery("select m from MODALIDADE m order by m.codModalidade desc", Modalidade.class).setMaxResults(1).getSingleResult().getCodModalidade() + 1;
        } catch (NoResultException e) {
            return Long.valueOf(1);
        }
    }

    public List<Modalidade> listAll() {
        List<Modalidade> md = em.createQuery("select m from MODALIDADE m", Modalidade.class).getResultList();
        return md;
    }

    public List<Modalidade> findModalidade() {
        List<Modalidade> modalidades = new ArrayList<>();
        try {
            modalidades = em.createQuery("select m from MODALIDADE m", Modalidade.class).getResultList();
        } catch (NoResultException ex) {
        }

        return modalidades;
    }

    public List<Modalidade> findModalidadeByTipoInstituicao(Long pCodTipo) {
        List<Modalidade> modalidades = new ArrayList<>();
        try {
            modalidades = em.createQuery("select m from MODALIDADE m join FETCH m.tipoInstituicao t where t.codTipoInstituicao=:codTipoInstituicao ", Modalidade.class)
                    .setParameter("codTipoInstituicao", pCodTipo)
                    .getResultList();
        } catch (NoResultException ex) {
        }

        return modalidades;
    }

    /*public List<Modalidade> findModalidadeByTipoInstituicaoTeste(Long pCodTipo) {
        List<Modalidade> modalidades = new ArrayList<>();

        try {
            /*String sql = "select m.nomeModalidade as nome, group_concat(p.pergunta) as pergunta, t.nomeTipoInstituicao as tipoInstituicao "
                    + "from MODALIDADE m "
                    + "inner join PERGUNTA p on m.codModalidade=p.modalidade_codModalidade "
                    + "inner join TIPOINSTITUICAO t on m.tipoInstituicao.codTipoInstituicao=t.codTipoInstituicao "
                    + "where t.codTipoInstituicao =:codTipoInstituicao "
                    + "group by m.codModalidade ";
            modalidades = (List<Modalidade>) em.createQuery(sql, Modalidade.class).
                    setParameter("codTipoInstituicao", pCodTipo);*/

            /*Query query = em.createNativeQuery("select m.nomeModalidade as nome, group_concat(p.pergunta) as pergunta, t.nomeTipoInstituicao as tipoInstituicao "
                    + "from MODALIDADE m "
                    + "inner join PERGUNTA p on m.codModalidade=p.modalidade_codModalidade "
                    + "inner join TIPOINSTITUICAO t on m.tipoInstituicao.codTipoInstituicao=t.codTipoInstituicao "
                    + "where t.codTipoInstituicao =1 "
                    + "group by m.codModalidade ");
            List<Object[]> list = query.getResultList();
            for (Object[] l : list) {

                Pergunta p = new Pergunta(l[0].toString(), l[1].toString());
                modalidades.add(p);
            }
        } catch (NoResultException e) {

        }
        return modalidades;

    }*/

    public Modalidade findModalidadeByID(Long pCodModalidade) {
        Modalidade m = new Modalidade();
        try {
            m = em.createQuery("select m from MODALIDADE m join FETCH m.tipoInstituicao t where m.codModalidade=:codModalidade", Modalidade.class)
                    .setParameter("codModalidade", pCodModalidade)
                    .getSingleResult();
        } catch (NoResultException ex) {
        }
        return m;
    }
}
