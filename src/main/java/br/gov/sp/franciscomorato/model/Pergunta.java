
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "PERGUNTA")
public class Pergunta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codPergunta;
    @NotEmpty
    @NotNull
    @NotBlank
    private String pergunta;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn
    private Modalidade modalidade;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private List<TipoResposta> tipoResposta = new ArrayList<>();

    public Pergunta() {
    }

    public Pergunta(String pergunta, String nomeModalidade) {
        this.pergunta = pergunta;
        this.modalidade.setNomeModalidade(nomeModalidade);
    }

    //@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.tipoResposta;
    }

    public String toString() {
        return "Pergunta{" + "codPergunta=" + codPergunta + ", pergunta=" + pergunta + ", modalidade=" + modalidade + '}';
    }
}
