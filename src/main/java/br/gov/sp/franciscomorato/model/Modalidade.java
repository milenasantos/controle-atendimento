/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "MODALIDADE")
public class Modalidade implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codModalidade;
    @NotEmpty
    @NotNull
    @NotBlank
    private String nomeModalidade;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn
    private TipoInstituicao tipoInstituicao;
    
    /*public Modalidade(){}
    
    public Modalidade(String nomeModalidade, String nomeTipoInstituicao){
        this.nomeModalidade = nomeModalidade;
        this.tipoInstituicao.setNomeTipoInstituicao(nomeTipoInstituicao);
        
    }*/
    
    @Override
    public String toString(){
        return "Modalidade{" + "codModalidade=" + codModalidade + ", nomeModalidade" + nomeModalidade + ", tipoInstituicao" + tipoInstituicao + '}';
    }
}
