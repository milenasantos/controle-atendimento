/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "ESTADO")
public class Estado implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long codEstado;
  @NotEmpty
  private String nomeEstado;
  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn
  @Valid
  private Pais pais;
  
  @Override
  public String toString()
  {
    return "Estado{" + "codEstado=" + codEstado + ", nomeEstado=" + nomeEstado + ", pais=" + pais.toString() + '}';
  }
}
