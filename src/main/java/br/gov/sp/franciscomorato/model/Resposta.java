/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "RESPOSTA")
public class Resposta implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codResposta;
    @NotEmpty
    @NotNull
    @NotBlank
    private String resposta;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    private Pergunta pergunta;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    private Registro registro;
    
    public Resposta(){}
    
    public Resposta(String resposta, Pergunta pergunta, Registro registro){
        this.resposta = resposta;
        this.pergunta = pergunta;
        this.registro = registro;
    }
    
    @Override
    public String toString(){
        return "Resposta{" + "codResposta=" + codResposta + ", resposta=" + resposta + ", pergunta=" + pergunta + '}';
    }
}
