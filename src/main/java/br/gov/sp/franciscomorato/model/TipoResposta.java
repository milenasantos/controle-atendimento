/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "TIPORESPOSTA")
public class TipoResposta implements GrantedAuthority {

    @Id
    private String nomeTipoResposta;

    @Override
    public String getAuthority() {
        return this.nomeTipoResposta;
    }

    @Override
    public String toString() {
        return "TipoResposta{" + ", nomeTipoResposta=" + nomeTipoResposta + '}';
    }

}
