/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "PAIS")
public class Pais implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long codPais;
  @NotEmpty
  private String nomePais;
  
  @Override
  public String toString()
  {
    return "Pais{" + "codPais=" + codPais + ", nomePais=" + nomePais + '}';
  }
}
