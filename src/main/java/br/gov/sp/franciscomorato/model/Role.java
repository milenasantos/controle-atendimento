/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "ROLE")
public class Role implements GrantedAuthority{

    @Id
    private String nome;
    
    @Override
    public String getAuthority(){
        return this.nome;
    }

    @Override
    public String toString() {
        return "Role{" + "nome=" + nome + '}';
    }
}
