/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "INSTITUICAO")
public class Instituicao implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codInstituicao;
    @NotEmpty
    @NotNull
    @NotBlank
    private String codIdInstituicao;
    @NotEmpty
    @NotNull
    @NotBlank
    private String nomeInstituicao;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn
    private TipoInstituicao tipoInstituicao;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    private Rua rua;
    @NotEmpty
    private String numero;
    @NotEmpty
    private String telefone;
    
    public String toString() {
        return "Instituicao{" + "codInstituicao=" + codInstituicao + ", codIdInstituicao=" + codIdInstituicao + ", nomeInstituicao=" + nomeInstituicao + ", tipoInstituicao=" + tipoInstituicao + ", rua=" + rua + ", numero=" + numero + ", telefone=" + telefone + '}';
    }
}
