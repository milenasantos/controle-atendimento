/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "REGISTRO")
public class Registro implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codRegistro;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataRegistro;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn
    private Instituicao instituicao;
    private String nivelProtecao;

    @Override
    public String toString() {
        return "Registro{" + "codRegistro=" + codRegistro + ", dataRegistro=" + dataRegistro + ", instituicao=" + instituicao + ", nivelProtecao=" + nivelProtecao + '}';
    }

    public Registro() {
    }

}