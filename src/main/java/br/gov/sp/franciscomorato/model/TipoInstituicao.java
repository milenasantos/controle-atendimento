/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author milena
 */
@Data
@Entity(name = "TIPOINSTITUICAO")
public class TipoInstituicao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codTipoInstituicao;
    @NotEmpty
    @NotNull
    @NotBlank
    private String nomeTipoInstituicao;

    @Override
    public String toString() {
        return "TipoInstituicao{" + "codTipoInstituicao=" + codTipoInstituicao + ", nomeTipoInstituicao=" + nomeTipoInstituicao + '}';
    }
}
