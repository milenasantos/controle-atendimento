/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.service;

import br.gov.sp.franciscomorato.model.Usuario;
import br.gov.sp.franciscomorato.repository.UsuarioDAO;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author milena
 */
@Service
public class UserService implements UserDetailsService
{
  @Autowired
  private UsuarioDAO DAO;
  
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
  {
    Usuario u = DAO.findUser(username);

    if(Objects.isNull(u))
    {
      throw new UsernameNotFoundException("Usuario" + u + "Não existe");
    }
    return u;
  }
}
