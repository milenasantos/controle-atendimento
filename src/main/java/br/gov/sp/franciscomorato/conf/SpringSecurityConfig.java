package br.gov.sp.franciscomorato.conf;

import br.gov.sp.franciscomorato.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 *
 * @author Jhonatan Colina
 */
/*
 * Classe de Configuracoes do Spring Security
 * Por padrao ele bloqueia tudo
 * Possui form de login padrao
 */ 
@EnableWebMvcSecurity
public class SpringSecurityConfig extends  WebSecurityConfigurerAdapter
{
  @Autowired
  private UserService userService;
  
  /*
  * Configura qual manager de autenticacao vai usar*/
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
  }
  
  /*libera/bloqueia as views, importante ter ROLE_ no banco como prefixo da Role*/
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
          .antMatchers("/home/**").permitAll()
          .antMatchers("/employee/**").permitAll()
          .antMatchers("/institution/**").permitAll()
          .antMatchers("/institutionType/**").permitAll()
          .antMatchers("/occupation/**").permitAll()
          .antMatchers("/user/**").permitAll()
          .antMatchers("/modality/**").permitAll()
          .antMatchers("/question/**").permitAll()
          .antMatchers("/record/**").permitAll()
          .antMatchers("/group/**").permitAll()
          .antMatchers("/").permitAll()
          .anyRequest().authenticated()
          .and()
          .formLogin().loginPage("/")
          .defaultSuccessUrl("/home").permitAll()
          .and()
          .logout()
          .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
          .logoutSuccessUrl("/").permitAll()
          .and()
          .exceptionHandling().accessDeniedPage("/WEB-INF/views/errors/403.jsp");
  }
  
  /*libera os resources (css,js)*/
  @Override
  public void configure(WebSecurity web) throws Exception {
          web.ignoring().antMatchers("/assets/**");
  }
}
