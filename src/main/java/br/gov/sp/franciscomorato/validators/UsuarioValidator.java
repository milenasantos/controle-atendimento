/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.validators;

import br.gov.sp.franciscomorato.model.Usuario;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author milena
 */
public class UsuarioValidator implements Validator {

    /*
  * Metodo para validar se esse validador é suportado pela classe
     */
    @Override
    public boolean supports(Class<?> type) {
        return Usuario.class.isAssignableFrom(type);
    }

    /*
    * As validações vão neste metodo
     */
    @Override
    public void validate(Object o, Errors errors) {
        Usuario u = (Usuario) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario", "campo.obrigatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwd", "campo.obrigatorio");
    }

}
