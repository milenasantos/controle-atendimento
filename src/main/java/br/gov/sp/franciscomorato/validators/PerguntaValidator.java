/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.gov.sp.franciscomorato.validators;

import br.gov.sp.franciscomorato.model.Pergunta;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author milena
 */
public class PerguntaValidator implements Validator{
    @Override
    public boolean supports(Class<?> type) {
        return Pergunta.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Pergunta p = (Pergunta) o;
    }
}
