<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<c:url value="/assets/images/logo_1.png"/>">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >  
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<c:url value='/assets/css/styles.css'/>">
        <!--datatable-->
        <link rel="stylesheet" href="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css"/>">
        <!---->
        <style>
            .thumb {
                width: 405px;
                height: 100%;
                border: thin solid #dedede;
                margin: 0px 15px 0px 0px;
            }

            .thumb.current {
                border: 1px solid #555;
            }

            .thumb .close {
                position: relative;
                float: right;
                font-weight: bold;
                font-size: 20px;
                padding: 1px;
                cursor: pointer;
            }

            desabilitado{
                pointer-events: none;
            }

            .thumb .info {
                text-align: center;
                font-weight: bold;
                background: white;
                border: none;
                padding: 5px 0;
            }        

            #thumbnail img {
                width: 403px;
                height: 100px;
            }

            .action {
                border: none;
                padding: 5px;
                margin: 5px;
                color: white;
            }
        </style>
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liPergunta"/>
                <jsp:param name="ativo1" value="liPergunta1"/>
            </jsp:include>
            <div style="margin: 15px">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" >
                    <div class="page-wrapper" >
                        <header class="page-heading clearfix" style="margin-top: -20px">
                            <h1 class="heading-title pull-left">Formul�rio de Edi��o de Modalidade</h1>
                        </header>
                        <div class="page-content">
                            <div class="row page-row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <c:url var="post_url" value="/modality/update"/>
                                    <form:form enctype="multipart/form-data" commandName="modalidade" class="form-horizontal" method="POST" action="${post_url}">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados da Modalidade</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <label class="control-label" for="codModalidade">*C�digo</label>
                                                        <form:input path="codModalidade" type="text" readonly="true"  class="form-control" name="codModalidade" id="codModalidade" />
                                                        <form:errors path="codModalidade" />
                                                    </div>
                                                    <div class="col-sm-10 col-md-10 col-lg-10">
                                                        <label class="control-label" for="nomeModalidade">*Nome</label>
                                                        <form:input path="nomeModalidade" type="text" required="true" readonly="true" class="form-control" name="nomeModalidade" id="nomeModalidade" />
                                                        <form:errors path="nomeModalidade" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div><div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Outras Informa��es</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                                        <label class="control-label" for="nomeTipoInstituicao">*Tipo de Institui��o</label>   
                                                        <form:select path="tipoInstituicao.codTipoInstituicao" required="true" readonly="true" id="tipoinstituicao" name="tipoInstituicao.codTipoInstituicao" class="form-control">
                                                            <form:options items="${tiposInstituicoes}" itemValue="codTipoInstituicao" itemLabel="nomeTipoInstituicao"/>
                                                        </form:select> 
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="pull-right" style="margin-right: 15px">
                                                        <a class="btn btn-default" type="button" href="<c:url value="/institution/list" />">Voltar</a>
                                                        <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="button" onclick="editar(this)" value="Editar">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form:form>
                                </div>
                            </div>
                        </div><!--//page-content-->
                    </div><!--//page--> 
                </div>
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
        <!--MASCARAS e CEP-->
        <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
        <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
        <!---->
        <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/page-data-tables.js" />"></script> 
        <!---->
        <!--MENU-->
        <script src="<c:url value="/assets/js/js-Menu.js" />" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
            function editar(b)
            {
                $('#nomeModalidade').removeAttr("readonly");
                $('#codTipoInstituicao').removeAttr("readonly");

                event.preventDefault();
                $(b).attr("onclick", "");
                $(b).attr("type", "submit");
                $(b).attr("value", "Atualizar");
            }

            function modal()
            {
                var titulo = 'Atualizar Modaliadade',
                        texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br /> Modalidade atualizada com �xito!',
                        funcao = '';
                $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
                $('#btn_modal').trigger('click');
            }
        </script>
    </body>
</html>
