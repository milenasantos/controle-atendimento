<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="pt">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="assets/images/logo_1.png">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/plugins/pretty-photo/css/prettyPhoto.css">    
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
        <!--datatable-->
        <link rel="stylesheet" href="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css">
        <!---->
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liFuncionario"/>
                <jsp:param name="ativo1" value="liFuncionario1"/>
            </jsp:include>
            <div style="margin: 15px">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" >
                    <div class="page-wrapper" >
                        <header class="page-heading clearfix" style="margin-top: -20px">
                            <h1 class="heading-title pull-left">Formul�rio de Cadastro do Funcion�rio</h1>
                        </header>
                        <div class="page-content">
                            <div class="row page-row">
                                <c:url var="post_url" value="/employee"/>
                                <form:form enctype="multipart/form-data" commandName="funcionario" class="form-horizontal" method="POST" action="${post_url}">
                                    <form:input path="situacao" name="situacao" type="hidden" value="true" />
                                    <form:input path="rua.bairro.cidade.estado.pais.nomePais" name="rua.bairro.cidade.estado.pais.nomePais" type="hidden" value="Brasil" />
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                            <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados do Funcion�rio</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-sm-2 col-md-2 col-lg-2">
                                                    <label class="control-label" for="codFuncionario">*C�digo</label>
                                                    <form:input path="codFuncionario" type="text" readonly="true"  class="form-control" name="codFuncionario" id="codFuncionario" />
                                                    <form:errors path="codFuncionario" />
                                                </div>
                                                <div class="col-sm-8 col-md-8 col-lg-8">
                                                    <label class="control-label" for="nomeCompleto">*Nome Completo</label>
                                                    <form:input path="nomeCompleto" type="text" required="true"  class="form-control" name="nomeCompleto" id="nomeCompleto" />
                                                    <form:errors path="nomeCompleto" />
                                                </div>
                                                <div class="col-sm-2 col-md-2 col-lg-2">
                                                    <label class="control-label" for="dataNascimento">*Data de Nascimento</label>
                                                    <form:input path="dataNascimento" required="true" type="text" onblur="validaData(this)" placeholder="__/__/____"  class="form-control" name="dataNascimento" id="dataNascimento" />
                                                    <form:errors path="dataNascimento" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                    <label class="control-label" for="rg">*RG</label>
                                                    <form:input path="rg" required="true" placeholder="__.___.___-_" type="text" class="form-control" name="rg" id="rg" />
                                                    <form:errors path="rg" />
                                                </div>
                                                <div class="col-sm-2 col-md-2 col-lg-2">
                                                    <label class="control-label" for="rg">*Org�o Emissor</label>
                                                    <form:input path="orgaoEmissor" required="true" placeholder="SSP-__" style="text-transform: uppercase;" type="text" class="form-control" name="orgaoEmissor" id="orgaoEmissor" />
                                                    <form:errors path="orgaoEmissor" />
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <label class="control-label" for="cpf">*CPF</label>
                                                    <form:input path="cpf" required="true" placeholder="___.___.___-__" type="text" class="form-control" name="cpf" id="cpf" />
                                                    <form:errors path="cpf" />
                                                </div>
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                    <label class="control-label" for="telefone">*Tel/Cel</label>
                                                    <form:input path="telefone" required="true" placeholder="(__) _____-____" type="text" class="form-control" name="telefone" id="telefone" />
                                                    <form:errors path="telefone" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                    <label class="control-label" for="sexo">*Sexo</label>
                                                    <form:input path="sexo" required="true" type="sexo" class="form-control" name="sexo" id="sexo" />
                                                    <form:errors path="sexo" />
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                    <label class="control-label">*Estado Civil</label>
                                                    <form:select path="estadoCivil" required="true" id="estadoCivil" name="estadoCivil" class="form-control">
                                                        <option value="solteiro" selected>Solteiro(a)</option>
                                                        <option value="casado">Casado(a)</option>
                                                        <option value="viuvo">Vi�vo(a)</option>
                                                        <option value="divorciado">Divorciado(a)</option>
                                                    </form:select>
                                                    <form:errors path="estadoCivil" />
                                                </div>
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                    <label class="control-label">*Escolaridade</label>
                                                    <form:select path="escolaridade" required="true" id="escolaridade" name="escolaridade" class="form-control">
                                                        <option value="false" selected>N�vel M�dio</option>
                                                        <option value="true">N�vel Superior</option>
                                                    </form:select>
                                                    <form:errors path="escolaridade"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                    <label class="control-label" for="email">*E-mail</label>
                                                    <form:input path="email" required="true" type="email" class="form-control" name="email" id="email" />
                                                    <form:errors path="email" />
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6">
                                                    <label class="control-label" for="confirmacaoEmail">*Confirma��o E-mail</label>
                                                    <input  type="email" class="form-control" required="true" name="confirmacaoEmail" id="confirmacaoEmail" onblur="verificaEmail()" />
                                                    <form:errors path="email" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                            <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Endere�o</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-sm-12 col-md-12 col-lg-12">
                                                    <g>Caso n�o saiba o CEP clique </g><a href="//www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">aqui.</a>       
                                                </div>                      
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-2">
                                                    <label class="control-label" for="cep">*CEP</label>
                                                    <form:input path="rua.cep" id="cep" required="true" maxlength="8" name="rua.cep" class="form-control" />
                                                    <form:errors path="rua.cep" />
                                                </div>
                                                <div class="col-md-8">
                                                    <label class="control-label" for="nomeRua">*Rua</label>
                                                    <form:input path="rua.nomeRua" required="true" id="logradouro" name="rua.nomeRua" maxlength="100" class="form-control" />
                                                    <form:errors path="rua.nomeRua" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="control-label" for="numeroCasa">*N�mero</label>
                                                    <form:input path="numeroCasa" required="true" id="numeroCasa" name="numeroCasa" maxlength="4" class="form-control" />
                                                    <form:errors path="numeroCasa" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label class="control-label">*Bairro</label>
                                                    <form:input path="rua.bairro.nomeBairro" required="true" id="bairro" name="rua.bairro.nomeBairro" maxlength="100" class="form-control" />
                                                    <form:errors path="rua.bairro.nomeBairro" />
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="control-label">*Cidade</label>
                                                    <form:input path="rua.bairro.cidade.nomeCidade" required="true" id="localidade" name="rua.bairro.cidade.nomeCidade" maxlength="100" class="form-control" />
                                                    <form:errors path="rua.bairro.cidade.nomeCidade" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="control-label">*Estado</label>
                                                    <form:select path="rua.bairro.cidade.estado.nomeEstado" required="true" id="uf" style="background: #eee; pointer-events: none; touch-action: none;" name="rua.bairro.cidade.estado.nomeEstado" class="form-control">
                                                        <option value="AC">AC</option>
                                                        <option value="AL">AL</option>
                                                        <option value="AP">AP</option>
                                                        <option value="AM">AM</option>
                                                        <option value="BA">BA</option>
                                                        <option value="CE">CE</option>
                                                        <option value="DF">DF</option>
                                                        <option value="ES">ES</option>
                                                        <option value="GO">GO</option>
                                                        <option value="MA">MA</option>
                                                        <option value="MT">MT</option>
                                                        <option value="MS">MS</option>
                                                        <option value="MG">MG</option>
                                                        <option value="PA">PA</option>
                                                        <option value="PB">PB</option>
                                                        <option value="PR">PR</option>
                                                        <option value="PE">PE</option>
                                                        <option value="PI">PI</option>
                                                        <option value="RJ">RJ</option>
                                                        <option value="RN">RN</option>
                                                        <option value="RS">RS</option>
                                                        <option value="RO">RO</option>
                                                        <option value="RR">RR</option>
                                                        <option value="SC">SC</option>
                                                        <option value="SP" selected="">SP</option>
                                                        <option value="SE">SE</option>
                                                        <option value="TO">TO</option>
                                                    </form:select>
                                                    <form:errors path="rua.bairro.cidade.estado.nomeEstado" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                            <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Informa��es Extras</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-lg-9">
                                                    <label class="control-label" for="nomeCargo">*Cargo</label>   
                                                    <form:select path="cargo.codCargo" required="true"  id="cargo" name="cargo.codCargo" class="form-control">
                                                        <form:options items="${cargos}" itemValue="codCargo" itemLabel="nomeCargo"/>
                                                    </form:select> 
                                                </div>
                                                <div class="col-sm-3 col-md-3 col-lg-3">
                                                    <label class="control-label">*Coordenador(a)</label>
                                                    <form:select path="coordenador" required="true" id="coordenador" name="coordenador" class="form-control">
                                                        <option value="false" selected>N�o</option>
                                                        <option value="true">Sim</option>
                                                    </form:select>
                                                    <form:errors path="coordenador"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="pull-right" style="margin-right: 15px">
                                                    <a class="btn btn-default" type="button" href="<c:url value="/home" />">Voltar</a>
                                                    <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Cadastrar">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div><!--//page-content-->
                    </div><!--//page--> 
                </div>
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->          
            <script type="text/javascript" src="assets/plugins/jquery-1.12.3.min.js"></script>
            <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
            <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
            <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
            <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
            <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
            <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
            <!--MASCARAS e CEP-->
            <script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>
            <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="assets/js/jquery.cookie.js"></script>
        <!---->
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js"></script> 
        <script type="text/javascript" src="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js"></script> 
        <script type="text/javascript" src="assets/js/datatable/page-data-tables.js"></script> 
        <!---->
        <!--MENU-->
        <script src="assets/js/js-Menu.js" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
            $(document).ready(function ()
            {
                $('#dataNascimento').mask('99/99/9999');
                $('#rg').mask('99.999.999-9');
                $('#cpf').mask('999.999.999-99');
                $('#orgaoEmissor').mask('SSP-**');
                $('#telefone').focusout(function ()
                {
                    var phone, element;
                    element = $(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if (phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');
            });

            function verificaEmail()
            {
                if ($('#email').val() !== "" && $('#confirmacaoEmail').val() !== "")
                {
                    if ($('#email').val() !== $('#confirmacaoEmail').val())
                    {
                        alert("Os campos de e-mail n�o conferem");
                        $('#confirmacaoEmail').val('');
                    }
                }
            }

            function modal()
            {
                var titulo = 'Inserir Funcion�rio',
                        texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br /> Funcion�rio cadastrado com �xito!',
                        funcao = '';
                $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
                $('#btn_modal').trigger('click');
            }
        </script>
    </body>
</html>
