<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<c:url value="/assets/images/logo_1.png"/>">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >  
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<c:url value='/assets/css/styles.css'/>">
        <!--datatable-->
        <link rel="stylesheet" href="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css"/>">
        <!---->
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liFuncionario"/>
                <jsp:param name="ativo1" value="liFuncionario1"/>
            </jsp:include>
            <div class="main" style="margin-top: 20px">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" style="margin: 15px;">
                    <header class="page-heading clearfix" style="margin-top: -20px">
                        <h1 class="heading-title pull-left">Lista de Funcionários</h1>
                    </header>
                    <div class="page-content">
                        <div class="row page-row">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                    <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Lista de Funcionários</h2>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive" style="overflow-x: hidden;">
                                        <table id="datatableFuncionario" class="table table-bordered" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 20px;">Código</th>
                                                    <th>Nome Completo</th>
                                                    <th>CPF</th>
                                                    <th>Telefone</th>
                                                    <th>E-mail</th>
                                                    <th>Situação</th>
                                                    <th>Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody align="center">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--//page-content-->
                </div><!--//page-->  
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
        <!--MASCARAS e CEP-->
        <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
        <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
        <!---->
        <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/page-data-tables.js" />"></script> 
        <!---->
        <!--MENU-->
        <script src="<c:url value="/assets/js/js-Menu.js" />" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
          $(document).ready(function ()
          {
              $('#datatableFuncionario').DataTable({
                  "pageLength": 25,
                  "bprocessing": true,
                  "aaData": ${lista},
                  "bPaginate": true,
                  "aoColumns": [
                      {"mData": "codFuncionario"},
                      {"mData": "nomeCompleto"},
                      {"mData": "cpf"},
                      {"mData": "telefone"},
                      {"mData": "email"},
                      {"mData": "situacao",
                          "mRender": function (data, type, row)
                          {
                              return (data === true ? "Ativo" : "Inativo");
                          }
                      },
                      //{"mData": "cargo.nomeCargo"},
                      {"mData": "codFuncionario",
                          "mRender": function (data, type, row)
                          {
                              var botoes = '<a class="btn btn-default btn-xs btn-flat" href="<c:url value="/employee/"/>' + data + '" title="Editar Funcionario" style="text-align:center" ><i class="fa fa-file" style="color: #333;"></i></a>';
                              return botoes;
                          }
                      }
                  ]
              });
          });

          /*function modal()
           {
           var titulo = 'Inserir Funcionário',
           texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br /> Funcionário cadastrado com êxito!',
           funcao = '';
           $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
           $('#btn_modal').trigger('click');
           }*/
        </script>
    </body>
</html> 
