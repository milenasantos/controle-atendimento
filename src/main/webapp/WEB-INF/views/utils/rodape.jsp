    </div>
    
  <footer class="footer" style="width:calc(100% - 220px); margin-left: 220px; overflow-x: hidden;background: #286090">
  <div class="footer-content">
      <div class="row">
        <div class="footer-col col-md-4 newsletter">
          <div class="footer-col about">
            <div class="footer-col-inner">
              <h3>Contato</h3>
              <ul>
                <li><i class="fa fa-phone" style="color: #fff"></i>(11) 4488-3305 | Ramal: 255</li>
                <li><i class="fa fa-home" style="color: #fff"></i>Rua Progresso, 700, Centro</li>
                <li><i class="fa fa-map-marker" style="color: #fff"></i>Cep: 07901-080</li>                                                            
                <!--<li style="width: 150%"><i class="fa fa-file" style="color: #fff"></i>CNPJ 046.523.072/0001-14</a></li>-->
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-col col-md-3 newsletter">
          <div class="footer-col about">
            <div class="footer-col-inner">
              <h3>Links �teis</h3>
              <ul>
                <a onclick="" style="cursor: pointer"><li><i class="fa fa-book" style="color: #fff"></i>Manual</li></a>                                                      
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-col col-md-5 newsletter">
          <div class="footer-col-inner">
            <img class="img-responsive" style="display: block;margin-left: auto; margin-right: auto; margin-top:11px;" src="${pageContext.request.contextPath}/assets/images/logo_rodape_3.png">
          </div><!--//footer-col-inner-->
        </div><!--//foooter-col--> 
      </div>       
  </div><!--//footer-content-->
  <div class="bottom-bar">
    <div class="container">
      <div class="row">
        <small class="copyright col-md-5 col-sm-12 col-xs-12">Prefeitura do Munic�pio de Francisco Morato @ 2017-2018</small>
        <small class="copyright col-md-4 col-sm-12 col-xs-12">� Sistema de Processo Web</small>
        <small class="copyright col-md-3 col-sm-12 col-xs-12" style="margin-left: -10px">Coordenadoria de Planejamento e TI</small>
      </div><!--//row--
      </div><!--//container-->
    </div><!--//bottom-bar-->
  </div>
</footer>
  
     </div>
</div><!--/row-offcanvas -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/assets/js/konami.js"/>"></script>