<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
    body,html,.row-offcanvas {
        height:100%;
    }

    body {
        padding-top: 50px;
    }

    #sidebar {
        width: inherit;
        min-width: 220px;
        max-width: 220px;
        background: url("<c:url value='/assets/images/bg.jpg'/>"); 
        border-color: #1a1c20 !important;
        border-right: solid 0.5px;
        float: left;
        height:100%;
        position:fixed;
        overflow-y:auto;
        overflow-x:hidden;
    }
    #main {
        height:100%;
        overflow:auto;
        width:calc(100% - 220px);
        margin-left: 220px;
    }

    .row-offcanvas {
        position: relative;
        -webkit-transition: all 0.25s ease-out;
        -moz-transition: all 0.25s ease-out;
        transition: all 0.25s ease-out;
    }

    .row-offcanvas-left.active
    {
        left: -220px;
        width:calc(100% + 220px);
        -webkit-transition: all 0.25s ease-out;
        -moz-transition: all 0.25s ease-out;
        transition: all 0.25s ease-out;
        position: relative;
    }

    .row-offcanvas-left {
        left: 0;
    }

    .sidebar-offcanvas {
        position: absolute;
        top: 0;
    }

    #menuLateral{

    }

    #menuLateral> li{
        color: #fff;
        border-bottom: 1px solid #1a1c20;
    }

    #menuLateral>  li > a {
        border-radius: 0;
        color: #c9d4f6;
        padding: 14px 18px 13px 15px;
        border-bottom: 1px solid #2f323a;
    }
    menuLateral > li:hover
    {
        background: #ddd;
    }

    #menuLateral>  li .active > a
    {
        background: #286090;
        color: #c9d4f6;
    }

    #menuLateral>  li > a:hover {
        color: #fff;
    }

    #menuLateral>  li > ul > li > a {
        color: #c9d4f6;
    }
    #menuLateral>  li > ul > li > a:hover {
        color: #fff;
    }

    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover
    {
        color: #fff !important;
    }


    /*
     * off Canvas sidebar
     * --------------------------------------------------
     */
    @media screen and (max-width: 768px) {
        .row-offcanvas {
            position: relative;
            -webkit-transition: all 0.25s ease-out;
            -moz-transition: all 0.25s ease-out;
            transition: all 0.25s ease-out;
            width:calc(100% + 220px);
        }

        .row-offcanvas-left
        {
            left: -220px;
        }

        .row-offcanvas-left.active {
            left: 0;
        }

        .sidebar-offcanvas {
            position: absolute;
            top: 0;
        }
    }
</style>
<nav class="navbar navbar-inverse navbar-fixed-top" id="menu" role="navigation" style="background: url('<c:url value='/assets/images/bg.jpg'/>'); border-color: #494646 !important">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="padding: 13px; padding-right: 15.5px; border-right: 0.5px solid #1a1c20;
               border-bottom: none;
               color: #FFF ;
               text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3) !important; 
               margin-top: -5px;  margin-bottom: -5px; font-size: 23.7px;
               font-family: 'Raleway', Helvetica, sans-serif !important; 
               font-weight: 200 !important"><span><img src="<c:url value="/assets/images/logo_1.png"/>" /></span>&nbsp;Registro Web</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li id="liInicio"><a href="<c:url value='/home'/>"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-home"></span>&nbsp;Inicio</a></li>
                <li id="liRegistro" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-file-text-o"></span>&nbsp;Registro&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="<c:url value='/record'/>" title="Adicionar Registro."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li class="divider"></li>
                        <li><a href="<c:url value='/record/list'/>" title="Listar Registros."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> 
                    </ul>
                </li>
                <li id="liPergunta" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-file-text-o"></span>&nbsp;Perguntas&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate multi-level" role="menu" style="cursor: pointer">
                        <li class="dropdown-header">Pergunta</li>
                        <li><a href="<c:url value='/question'/>" title="Adicionar Pergunta."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                        <//sec:authorize access="hasRole('ROLE_ROOT')">
                        <li><a href="<c:url value='/question/list'/>" title="Listar Perguntas."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                        <li class="divider"></li>
                        <li class="dropdown-header">Modalidade</li>
                        <li><a href="<c:url value='/modality'/>" title="Adicionar Modalidades."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li><a href="<c:url value='/modality/list'/>" title="Listar Modalidades."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                    </ul>
                </li>
                <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                <li id="liFuncionario" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-user-plus"></span>&nbsp;Funcion�rio&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="<c:url value='/employee'/>" title="Adicionar Funcion�rio."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li class="divider"></li>
                        <li><a href="<c:url value='/employee/list'/>" title="Listar Funcion�rios."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> 
                    </ul>
                </li>
                <li id="liCargo" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-user-plus"></span>&nbsp;Cargo&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="<c:url value='/occupation'/>" title="Adicionar Cargo."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li class="divider"></li>
                        <li><a href="<c:url value='/occupation/list'/>" title="Listar Cargos."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <!--
                        <li><a href="<//c:url value='/occupation/list'/>" title="Listar Cargos."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> -->
                    </ul>
                </li>
                <li id="liInstituicao" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-file-text-o"></span>&nbsp;Institui��o&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate multi-level" role="menu" style="cursor: pointer">
                        <li class="dropdown-header">Institui��o</li>
                        <li><a href="<c:url value='/institution'/>" title="Adicionar Institui��o."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                        <//sec:authorize access="hasRole('ROLE_ROOT')">
                        <li><a href="<c:url value='/institution/list'/>" title="Listar Institui��es."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                        <li class="divider"></li>
                        <li class="dropdown-header">Tipo de Institui��o</li>
                        <li><a href="<c:url value='/institutionType'/>" title="Adicionar Tipos de Institui��es."><span class="fa fa-plus"></span>&nbsp;Adicionar/Listar</a></li>
                        <li><a href="<c:url value='/institutionType/list'/>" title="Listar Tipos de Institui��es."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                    </ul>
                </li>
                <li id="liUsuario" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-users"></span>&nbsp;Usu�rio&nbsp;<span class="caret"></span></a>
                    <ul class="dropdown-menu forAnimate" role="menu">
                        <li><a href="<c:url value='/user'/>" title="Adicionar Usu�rio."><span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li class="divider"></li>
                        <li><a href="<c:url value='/user/list'/>" title="Listar Usu�rios."><span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> 
                    </ul>
                </li>
                <//sec:authorize>
                <!--  <li id="liUsuarios" class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-users"></span>&nbsp;Usu�rios&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu forAnimate" role="menu">
                  <li class="dropdown-header">Usu�rio</li>
                  <li><a href="frm_adicionarUsuario.jsp" title="Adicionar Usu�rio."><span class="fa fa-plus"></span>&nbsp;Adicionar/Listar</a></li>
                </ul>
              </li> -->
            </ul>
            <ul class="nav navbar-right navbar-nav user-nav" style="float: right !important">
                <//sec:authorize access="isAuthenticated()">
                <//sec:authentication property="principal" var="user"/>
                <li id="liNomeUsuario" class="dropdown profile_menu"><a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true"><span id="iniciais" data-letters="${fn:toUpperCase(fn:substring(user.username,0,1))}">${fn:toUpperCase(fn:substring(user.username,0,1))}${fn:substring(user.username,1,100)}</span><b class="caret"></b></a>
                    <ul class="dropdown-menu" style="cursor: pointer">
                        <!-- <li><a href="frm_trocarSenha.jsp">Trocar Senha</a></li>
                        <li class="divider"></li> -->
                        <c:url var="urlToLogout" value="/logout"/>
                        <li><a href="${urlToLogout}">Fazer Logoff</a></li>
                    </ul>
                </li>
                <//sec:authorize>
            </ul>
        </div>
    </div>
</nav>
<div class="row-offcanvas row-offcanvas-left">
    <div id="sidebar" class="sidebar-offcanvas">
        <div class="col-md-12" style="margin-top: 51px;padding: 0;">
            <ul id="menuLateral" class="nav nav-pills nav-stacked nav-list">
                <li id="liInicio1" ><a href="<c:url value='/home'/>"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-home"></span>&nbsp;Inicio</a></li>
                <li id="liPergunta1" class="nav-header" data-toggle="collapse" data-target="#test4">
                    <a style="cursor: pointer"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-file-text-o"></span>&nbsp;Perguntas&nbsp;<span class="caret pull-right" style="margin-top: 5px; border-width: 6px 5px 6px 5px;"></span></a>
                    <ul class="nav nav-list collapse multi-level" id="test4">
                        <li class="dropdown-header">Pergunta</li>
                        <li><a href="<c:url value='/question'/>" title="Adicionar Pergunta.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                        <li><a href="<c:url value='/question/list'/>" title="Listar Perguntas.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> 
                        <li class="divider"></li>
                        <li class="dropdown-header">Modalidade</li>
                        <li><a href="<c:url value='/question'/>" title="Adicionar Modalidades.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li><a href="<c:url value='/question/list'/>" title="Listar Modalidades.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                    </ul>
                </li>
                <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                <li id="liFuncionario1" class="nav-header" data-toggle="collapse" data-target="#test">
                    <a style="cursor: pointer"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-user-plus"></span>&nbsp;Funcion�rio&nbsp;<span class="caret pull-right" style="margin-top: 5px; border-width: 6px 5px 6px 5px;"></span></a>
                    <ul class="nav nav-list collapse" id="test">
                        <li><a href="<c:url value='/employee'/>" title="Adicionar Funcion�rio.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li><a href="<c:url value='/employee/list'/>" title="Listar Funcion�rios.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                    </ul>
                </li>
                <li id="liCargo1" class="nav-header" data-toggle="collapse" data-target="#test3">
                    <a style="cursor: pointer"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-user-plus"></span>&nbsp;Cargo&nbsp;<span class="caret pull-right" style="margin-top: 5px; border-width: 6px 5px 6px 5px;"></span></a>
                    <ul class="nav nav-list collapse" id="test3">
                        <li><a href="<c:url value='/occupation'/>" title="Adicionar Cargo.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li><a href="<c:url value='/occupation/list'/>" title="Listar Cargos.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <!--<li><a href="<//c:url value='/occupation/list'/>" title="Listar Cargos.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>-->
                    </ul>
                </li>
                <li id="liInstituicao1" class="nav-header" data-toggle="collapse" data-target="#test4">
                    <a style="cursor: pointer"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-file-text-o"></span>&nbsp;Institui��o&nbsp;<span class="caret pull-right" style="margin-top: 5px; border-width: 6px 5px 6px 5px;"></span></a>
                    <ul class="nav nav-list collapse multi-level" id="test4">
                        <li class="dropdown-header">Institui��o</li>
                        <li><a href="<c:url value='/institution'/>" title="Adicionar Institui��o.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <//sec:authorize access="hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')">
                        <li><a href="<c:url value='/institution/list'/>" title="Listar Institui��es.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li> -->
                        <li class="divider"></li>
                        <li class="dropdown-header">Tipo de Institui��o</li>
                        <li><a href="<c:url value='/institutionType'/>" title="Adicionar Tipos de Institui��es.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar/Listar</a></li>
                        <li><a href="<c:url value='/institutionType/list'/>" title="Listar Tipos de Institui��es.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                        <//sec:authorize>
                    </ul>
                </li>
                <li id="liUsuario1" class="nav-header" data-toggle="collapse" data-target="#test2">
                    <a style="cursor: pointer"><span style="font-size:16px;" class="hidden-xs showopacity fa fa-users"></span>&nbsp;Usu�rio&nbsp;<span class="caret pull-right" style="margin-top: 5px; border-width: 6px 5px 6px 5px;"></span></a>
                    <ul class="nav nav-list collapse" id="test2">
                        <li><a href="<c:url value='/user'/>" title="Adicionar Usu�rio.">&nbsp;&nbsp;<span class="fa fa-plus"></span>&nbsp;Adicionar</a></li>
                        <li><a href="<c:url value='/user/list'/>" title="Listar Usu�rios.">&nbsp;&nbsp;<span class="fa fa-list-ul"></span>&nbsp;Listar</a></li>
                    </ul>
                </li>
                <//sec:authorize>
            </ul>
        </div>
    </div>
    <div id="main">
        <p style="margin-top: 1px">
            <button onclick="changeChevron()" type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><i id="seta" class="glyphicon glyphicon-chevron-left"></i></button>
        </p>
        <div class="col-md-12">
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
            <script>
                $(document).ready(function ()
                {
                    $('#<%=request.getParameter("ativo")%>').addClass('active');
                    $('#<%=request.getParameter("ativo1")%>').addClass('active');
                    $('[data-toggle=offcanvas]').click(function () {
                        $('.row-offcanvas').toggleClass('active');
                    });
                });

                function changeChevron()
                {
                    if ($('#seta').attr('class') === "glyphicon glyphicon-chevron-left")
                    {
                        $('#seta').removeClass();
                        $('#seta').addClass("glyphicon glyphicon-chevron-right");
                    } else
                    {
                        $('#seta').removeClass();
                        $('#seta').addClass("glyphicon glyphicon-chevron-left");
                    }
                }
            </script>