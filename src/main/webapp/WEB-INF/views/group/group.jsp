<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<c:url value="/assets/images/logo_1.png"/>">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >  
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<c:url value='/assets/css/styles.css'/>">
        <!--datatable-->
        <link rel="stylesheet" href="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css"/>">
        <!---->
        <style>
            .thumb {
                width: 405px;
                height: 100%;
                border: thin solid #dedede;
                margin: 0px 15px 0px 0px;
            }

            .thumb.current {
                border: 1px solid #555;
            }

            .thumb .close {
                position: relative;
                float: right;
                font-weight: bold;
                font-size: 20px;
                padding: 1px;
                cursor: pointer;
            }

            desabilitado{
                pointer-events: none;
            }

            .thumb .info {
                text-align: center;
                font-weight: bold;
                background: white;
                border: none;
                padding: 5px 0;
            }        

            #thumbnail img {
                width: 403px;
                height: 100px;
            }

            .action {
                border: none;
                padding: 5px;
                margin: 5px;
                color: white;
            }
        </style>
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liRegistro"/>
                <jsp:param name="ativo1" value="liRegistro1"/>
            </jsp:include>
            <div class="main">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" style="margin: 15px;">
                    <header class="page-heading clearfix" style="margin-top: -20px">
                        <h1 class="heading-title pull-left">Formul�rio de Cadastro de Grupo ao Registro</h1>
                    </header>
                    <div class="page-content">
                        <div class="row page-row">
                            <c:url var="post_url" value="/group"/>
                            <form:form enctype="multipart/form-data" commandName="grupo" class="form-horizontal" method="POST" action="${post_url}">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                        <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Grupos de Atividade</h2>
                                    </div>
                                    <div class="panel-body">
                                        <form:input path="registro.codRegistro" name="pCodRegistro" id="pCodRegistro"/>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="data">*Data</label>
                                                <form:input path="data" required="true" type="text" onblur="validaData(this)" placeholder="__/__/____"  class="form-control" name="data" id="data" />
                                                <form:errors path="data" />
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="qtdeParticipantes">*N� de Participantes</label>
                                                <form:input path="qtdeParticipantes" required="true" type="number" class="form-control" name="qtdeParticipantes" id="qtdeParticipantes" />
                                                <form:errors path="qtdeParticipantes" />
                                            </div>
                                            <div class="col-sm-8 col-md-8 col-lg-8">
                                                <label class="control-label" for="qtdeParticipantes">*Institui��o</label>
                                                <form:input path="registro.instituicao.nomeInstituicao" readonly="true" type="text" class="form-control" id="nomeInstituicao" name="registro.instituicao.nomeInstituicao"/>
                                                <form:errors path="registro.instituicao.nomeInstituicao"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <label class="control-label" for="nomeTipoGrupo">*Tipo de Grupo</label>   
                                                <form:select path="tipoGrupo.codTipoGrupo" required="true"  id="tipogrupo" name="tipoGrupo.codTipoGrupo" class="form-control">
                                                    <form:options items="${tiposGrupos}" itemValue="codTipoGrupo" itemLabel="nomeTipoGrupo"/>
                                                </form:select> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <label class="control-label" for="temaAbordado">*Tema Abordado</label>   
                                                <form:textarea style="resize: vertical" rows="4" path="temaAbordado" id="temaAbordado" name="temaAbordado" class="form-control"></form:textarea>
                                                <form:errors path="temaAbordado" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <label class="control-label" for="cep">*Funcion�rio Respons�vel</label>   
                                                <select required="true"  id="tipogrupo" name="selectFuncionario" class="form-control">
                                                    <c:forEach var="funcionario" items="${funcs}">
                                                        <option value="${funcionario.codFuncionario}">${funcionario.nomeCompleto}</option>
                                                    </c:forEach>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="pull-right" style="margin-right: 15px">
                                                <a class="btn btn-default" type="button" href="<c:url value="/record" />">Voltar</a>
                                                <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Cadastrar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div><!--//page-content-->
                </div><!--//page-->  
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
        <!--MASCARAS e CEP-->
        <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
        <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
        <!---->
        <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/page-data-tables.js" />"></script> 
        <!---->
        <!--MENU-->
        <script src="<c:url value="/assets/js/js-Menu.js" />" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
        $(document).ready(function ()
        {
            $('#data').mask('99/99/9999');
        });
        </script>
    </body>
</html>