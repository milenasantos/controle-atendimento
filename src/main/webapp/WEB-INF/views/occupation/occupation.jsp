<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="assets/images/logo_1.png">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/plugins/pretty-photo/css/prettyPhoto.css">    
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
        <!--datatable-->
        <link rel="stylesheet" href="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css">
        <!---->
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liCargo"/>
                <jsp:param name="ativo1" value="liCargo1"/>
            </jsp:include>
            <div style="margin: 15px">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" >
                    <div class="page-wrapper" >
                        <header class="page-heading clearfix" style="margin-top: -20px">
                            <h1 class="heading-title pull-left">Formul�rio de Cadastro de Cargo</h1>
                        </header>
                        <div class="page-content">
                            <div class="row page-row">
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <c:url var="post_url" value="/occupation"/>
                                    <form:form enctype="multipart/form-data" commandName="cargo" class="form-horizontal" method="POST" action="${post_url}">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados do Cargo</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <label class="control-label" for="codCargo">*C�digo</label>
                                                        <form:input path="codCargo" type="text" readonly="true"  class="form-control" name="codCargo" id="codCargo" />
                                                        <form:errors path="codCargo" />
                                                    </div>
                                                    <div class="col-sm-10 col-md-10 col-lg-10">
                                                        <label class="control-label" for="nomeCargo">*Nome</label>
                                                        <form:input path="nomeCargo" type="text" required="true"  class="form-control" name="nomeCargo" id="nomeCargo" />
                                                        <form:errors path="nomeCargo" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div><div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Outras Informa��es</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-12 col-sm-12 col-lg-12">
                                                        <label class="control-label" for="nomeInstituicao">Selecione a Institui��o</label>   
                                                        <form:select path="instituicao.codInstituicao" required="true"  id="instituicao" name="instituicao.codInstituicao" class="form-control">
                                                            <form:options items="${istituicoes}" itemValue="codInstituicao" itemLabel="nomeInstituicao"/>
                                                        </form:select> 
                                                        <form:errors path="instituicao.codInstituicao" />
                                                    </div>
                                                    <!--<div class="col-md-8 col-sm-8 col-lg-8">
                                                        <label class="control-label">Institui��o</label>   
                                                        <//form:select path="instituicao.codIstituicao" required="true"  id="instituicao" name="instituicao.codIstituicao" class="form-control">
                                                            <//form:options items="${instituicoes}" itemValue="codIstituicao" itemLabel="nomeInstituicao"/>
                                                        <//form:select> 
                                                    </div>-->
                                                </div>
                                                <div class="form-group">
                                                    <div class="pull-right" style="margin-right: 15px">
                                                        <a class="btn btn-default" type="button" href="<c:url value="/home" />">Voltar</a>
                                                        <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Cadastrar">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form:form>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                            <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Lista de Cargos</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive" style="overflow-x: hidden;">
                                                <table id="datatableCargos" class="table table-bordered" >
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 20px;">C�digo</th>
                                                            <th>Nome</th>
                                                            <th>Institui��o</th>
                                                            <th>Op��es</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody align="center">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--//page-content-->
                    </div><!--//page--> 
                </div>
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->          
            <script type="text/javascript" src="assets/plugins/jquery-1.12.3.min.js"></script>
            <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
            <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
            <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
            <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
            <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
            <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
            <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
            <!--MASCARAS e CEP-->
            <script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>
            <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="assets/js/jquery.cookie.js"></script>
        <!---->
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js"></script> 
        <script type="text/javascript" src="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js"></script> 
        <script type="text/javascript" src="assets/js/datatable/page-data-tables.js"></script> 
        <!---->
        <!--MENU-->
        <script src="assets/js/js-Menu.js" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
        $(document).ready(function ()
        {
            $('#datatableCargos').DataTable({
                "pageLength": 25,
                "bprocessing": true,
                "aaData": ${lista},
                "bPaginate": true,
                "aoColumns": [
                    {"mData": "codCargo"},
                    {"mData": "nomeCargo"},
                    {"mData": "instituicao.nomeInstituicao"},
                    {"mData": "codCargo",
                        "mRender": function (data, type, row)
                        {
                            var botoes = '<a class="btn btn-default btn-xs btn-flat" href="<c:url value="/occupation/"/>' + data + '" title="Editar Cargo" style="text-align:center" ><i class="fa fa-file" style="color: #333;"></i></a>';
                            return botoes;
                        }
                    }
                ]
            });
        });

        function modal()
        {
            var titulo = 'Inserir Cargo',
                    texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br/>Cargo cadastrado com �xito!',
                    funcao = '';
            $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
            $('#btn_modal').trigger('click');
        }
        </script>
    </body>
</html>
