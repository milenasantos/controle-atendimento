<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<c:url value="/assets/images/logo_1.png"/>">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >  
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<c:url value='/assets/css/styles.css'/>">
        <!--datatable-->
        <link rel="stylesheet" href="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css"/>">
        <!---->
        <!-- Steps -->
        <link rel="stylesheet" href="<c:url value='/assets/css/steps/jquery.steps.css'/>">
        <style>
            .thumb {
                width: 405px;
                height: 100%;
                border: thin solid #dedede;
                margin: 0px 15px 0px 0px;
            }

            .thumb.current {
                border: 1px solid #555;
            }

            .thumb .close {
                position: relative;
                float: right;
                font-weight: bold;
                font-size: 20px;
                padding: 1px;
                cursor: pointer;
            }

            desabilitado{
                pointer-events: none;
            }

            .thumb .info {
                text-align: center;
                font-weight: bold;
                background: white;
                border: none;
                padding: 5px 0;
            }        

            #thumbnail img {
                width: 403px;
                height: 100px;
            }

            .action {
                border: none;
                padding: 5px;
                margin: 5px;
                color: white;
            }
        </style>
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liRegistro"/>
                <jsp:param name="ativo1" value="liRegistro1"/>
            </jsp:include>
            <div class="main">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" style="margin: 15px;">
                    <header class="page-heading clearfix" style="margin-top: -20px">
                        <h1 class="heading-title pull-left">Formul�rio Gerador de Registro Mensal</h1>
                    </header>
                    <div class="page-content">
                        <div class="row page-row">
                            <c:url var="post_url" value="/record"/>
                            <form:form enctype="multipart/form-data" commandName="registro" class="form-horizontal" method="POST" action="${post_url}">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                        <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados do Registro</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="codRegistro">*C�digo do Registro</label>
                                                <form:input path="codRegistro" type="text" readonly="true" class="form-control" name="codRegistro" id="codRegistro" />
                                                <form:errors path="codRegistro" />
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="codInstituicao">*C�digo da Institui��o</label>
                                                <form:input readonly="true" path="instituicao.codInstituicao" type="text" class="form-control" name="instituicao.codInstituicao" id="instituicao.codInstituicao" />
                                            </div>
                                            <div class="col-sm-8 col-md-8 col-lg-8">
                                                <label class="control-label" for="nomeInstituicao">*Nome da Institui��o</label>
                                                <form:input path="instituicao.nomeInstituicao" type="text" readonly="true" required="true"  class="form-control" name="instituicao.nomeInstituicao" id="nomeInstituicao" />
                                                <form:errors path="instituicao.nomeInstituicao" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="dataRegistro">*Data do Registro</label>
                                                <form:input path="dataRegistro" required="true" readonly="true" type="text" onblur="validaData(this)" placeholder="__/__/____"  class="form-control" name="dataRegistro" id="dataRegistro" />
                                                <form:errors path="dataRegistro" />
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <label class="control-label" for="nivelProtecao">*N�vel de Prote��o</label>
                                                <form:input path="nivelProtecao" type="text" required="true"  class="form-control" name="nivelProtecao" id="nivelProtecao" />
                                                <form:errors path="nivelProtecao" />
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-lg-8">
                                                <label class="control-label" for="nomeTipoInstituicao">*Tipo de Institui��o</label>   
                                                <form:input path="instituicao.tipoInstituicao.nomeTipoInstituicao" type="text" readonly="true" required="true"  class="form-control" name="instituicao.tipoInstituicao.nomeTipoInstituicao" id="nomeTipoInstituicao" />
                                                <form:errors path="instituicao.tipoInstituicao.nomeTipoInstituicao" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                        <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Responda as Perguntas Abaixo</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <div class="ibox-content">
                                                    <div id="wizard" class="form-horizontal">
                                                        <h1></h1>
                                                         <div class="step-content" style="padding-top: 10px; ">
                                                            <c:forEach var="modalidade" items="${modalidades}">
                                                                <h4><u style="font-weight: 600">Modalidade: ${modalidade.nomeModalidade}</u></h4>
                                                                <input type="text" name="pCodModalidade" id="pCodModalidade" value="${modalidade.codModalidade}">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12 col-md-12 col-lg-12" style="font-weight: 500">
                                                                        <h5 style="font-weight: 600"><br>A - Volume de fam�lias em acompanhamento no Equipamento</h5>
                                                                    </div>
                                                                </div>
                                                                <div id="listaPerguntas">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                                            <input class="form-control" type="number" name="1" id="input-1">
                                                                        </div>
                                                                    </div>
                                                                    <!--
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                                            <textarea style="resize: vertical" rows="5"  class="form-control" maxlength="200" id="input-" name="" ></textarea>
                                                                        </div>
                                                                    </div>
                                                                    -->
                                                                </div>
                                                            </c:forEach> 
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="panel panel-default">
                                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                        <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Grupos de Atividade</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <label class="control-label" for="nomeTipoGrupo">*Tema Abordado</label>   
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                <label class="control-label" for="nomeTipoGrupo">*Tipo de Grupo</label>   
                                                <select required="true"  id="tipogrupo" name="selectSbrug" class="form-control">
                                                    <//c:forEach var="tipoGrupo" items="${tiposGrupos}">
                                                        <option value="${tipoGrupo.codTipoGrupo}">${tipoGrupo.nomeTipoGrupo}</option>
                                                    <//c:forEach>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="pull-right" style="margin-right: 15px">
                                                <a class="btn btn-default" type="button" href="<c:url value="/institution/list" />">Voltar</a>
                                                <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Gerar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div><!--//page-content-->
                </div><!--//page-->  
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
        <!--MASCARAS e CEP-->
        <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
        <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
        <!---->
        <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/page-data-tables.js" />"></script> 
        <!---->
        <!--MENU-->
        <script src="<c:url value="/assets/js/js-Menu.js" />" type="text/javascript" charset="utf-8"></script>
        <!---->
        <!-- Steps -->
        <script type="text/javascript" src="<c:url value="/assets/js/steps/jquery.steps.min.js" />"></script> 
        <script>
        $(document).ready(function ()
        {
            var wizard = $("#wizard").steps({
                onFinished: function (event, currentIndex)
                {
                    $('#btnFinalizar').trigger('click');
                    return true;
                },
                onStepChanging: function (event, currentIndex) {
                    return true;
                }
            });

            /*$('input').on('ifChecked', function (e) {
             var id = e['currentTarget']['id'];
             if ($('#' + id).attr('type') === "checkbox")
             {
             var divDePerguntas = $('#' + id).attr('divAtual');
             var inputs = document.getElementById(divDePerguntas).getElementsByTagName('input');
             
             for (i = 0; i < inputs.length; i++)
             {
             if (inputs[i].id !== id)
             $('#' + inputs[i].id).iCheck('disable');
             }
             }
             });
             
             $('input').on('ifUnchecked', function (e) {
             var id = e['currentTarget']['id'];
             if ($('#' + id).attr('type') === "checkbox")
             {
             var divDePerguntas = $('#' + id).attr('divAtual');
             var inputs = document.getElementById(divDePerguntas).getElementsByTagName('input');
             
             for (i = 0; i < inputs.length; i++)
             {
             $('#' + inputs[i].id).iCheck('enable');
             }
             }
             });*/
            data();
        });

        function data()
        {
            var data = new Date();
            $('#dataRegistro').val((data.getDate() <= 9 ? "0" + data.getDate() : data.getDate()) + "/" + ((data.getMonth() + 1) <= 9 ? "0" + (data.getMonth() + 1) : (data.getMonth() + 1)) + "/" + data.getFullYear());
        }

        function modal()
        {
            var titulo = 'Inserir Registro',
                    texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br/>Registro cadastrado com �xito!',
                    funcao = '';
            $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
            $('#btn_modal').trigger('click');
        }
        </script>
    </body>
</html>