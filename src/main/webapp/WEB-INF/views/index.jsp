
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
  <head>
    <title>Prefeitura do Munic�pio de Francisco Morato</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="<c:url value='/assets/images/logo_1.png'/>">  
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
    <!-- Global CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/css/styles.css'/>">
    <link rel="stylesheet" href="<c:url	value='/assets/css/loginPMFM.css'/>" >
  </head>
  <body class="bodyLoginPMFM">
    <div class="wrapper">
      <!-- ******CONTENT****** --> 
      <div class="content container containerLoginPMFM">
        <div class="page-wrapper"> 
          <div class="page-content">
            <div class="row page-row col-sm-5 col-md-5 col-lg-5 rowLoginPMFM" >
              <div class="panel panel-default panelLoginPMFM">
                <div class="panel-heading panelHeadingPMFM">
                  <h2><img src="<c:url value='/assets/images/logo_1.png'/>" width="50" alt="logo" class="logo-img">&nbsp;Registro Mensal de Atendimento</h2>
                </div>
                <c:url var="post_url"  value="/login" />
                <form:form servletRelativeAction="/" class="form-horizontal" method="POST">
                  <div class="panel-body">
                    <div class="form-group">
                      <div class="col-sm-12 col-md-12 col-lg-12">
                        <h4 class="fraseLoginPMFM">Fa�a Login para acessar o Sistema</h4>
                      </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 25px">
                      <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                          <input id="username" required="" name="username" type="text" placeholder="Usu�rio" class="form-control inputLoginPMFM"/>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                          <input id="password" name="password" required="" type="password" placeholder="***" class="form-control inputLoginPMFM" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="text-align: right">
                      <c:if test="${param.error != null}">
                      <div class="col-sm-8 col-md-8 col-lg-8">
                        <p id="loginInvalido">Usu�rio e(ou) Senha Inv�lidos!</p>
                      </div>
                      </c:if>
                      <div class="<c:if test="${param.error == null}"> col-sm-offset-8 col-md-offset-8 col-lg-8 </c:if>col-sm-4 col-md-4 col-lg-4">
                        <input type="submit" class="btn btn-theme botaoLoginPMFM" value="Entrar" name="btn_entrar" />
                      </div>
                    </div>
                  </div>
                </form:form>
              </div>
              <div class="text-center out-links"><a href="http://www.franciscomorato.sp.gov.br" class="rodapeLoginPMFM">� 2018 Prefeitura Municipal de Francisco Morato</a></div>
            </div><!--//page-content-->
          </div><!--//page--> 
        </div><!--//content-->
        <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
        <div id="modal"></div>
      </div>
    </div><!--//wrapper-->
      <!-- Javascript -->
      <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
      <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
      <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
      <!--MASCARAS e CEP-->
      <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
      <!---->
      <!-- cookie -->
      <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
      <!---->
      <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
      <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
      <script>
      $(document).ready(function ()
      {
         $('#username').focus();
         if($('.campoInvalido').html() === "")
          $('.campoInvalido').remove();
         else
          $('.campoInvalido').effect( "shake",{times:3,distance:10},600 );
         
         if(${param.error != null})
         {
          $('#loginInvalido').effect( "shake",{times:3,distance:10},600 );
         }
      });
      </script>
  </body>
</html> 
