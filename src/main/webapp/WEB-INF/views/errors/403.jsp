<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<c:url value='/assets/images/logo_1.png' />">
    <title>Sistema de Processo Web</title>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
    <!-- Global CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
    <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<c:url	value='/assets/css/styles.css'/>">
    <link rel="stylesheet" href="<c:url	value='/assets/css/loginPMFM.css'/>" >
  </head>
  <body class="bodyLoginPMFM">
    <div id="cl-wrapper" class="error-container">
      <div class="page-error" style="margin-top: 80px; margin-bottom: 40px">
        <h1 class="number text-center" style="color: #FFF; font-size: 150px; font-family: Arial; text-shadow: 1px 1px 5px rgba(0, 0, 0, 0.6)">403</h1>
        <h2 class="description text-center" style="color: #FFF; font-size: 40px; text-shadow: 1px 1px 5px rgba(0, 0, 0, 0.6)">Desculpe, mas voc� n�o tem permiss�o de acesso! :(${message}</h2>
        <h3 class="text-center" style="color: #FFF;text-shadow: 1px 1px 5px rgba(0, 0, 0, 0.6) ">Me leve de volta ao <a href="<c:url value="/home" />">In�cio </a>!</h3>
      </div>
      <div class="text-center copy" style="color:#C9D4F6; text-shadow:1px 1px 0 rgba(0, 0, 0, 0.3) ">� 2018 <a href="#" style="color:#C9D4F6; text-shadow:1px 1px 0 rgba(0, 0, 0, 0.3)">Prefeitura Municipal de Francisco Morato</a></div>
    </div>
    <script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/jquery.nanoscroller/javascripts/jquery.nanoscroller.js"></script>
    <script type="text/javascript" src="assets/js/cleanzone.js"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/voice-recognition.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      });
      
    </script>
  </body>
</html>
