<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Registro Mensal de Atendimento</title>
        <!-- Meta -->
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<c:url value="/assets/images/logo_1.png"/>">  
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
        <!-- Global CSS -->
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/bootstrap/css/bootstrap.min.css'/>">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/font-awesome/css/font-awesome.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/flexslider/flexslider.css'/>" >
        <link rel="stylesheet" href="<c:url	value='/assets/plugins/pretty-photo/css/prettyPhoto.css'/>" >  
        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<c:url value='/assets/css/styles.css'/>">
        <!--datatable-->
        <link rel="stylesheet" href="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css"/>">
        <!---->
        <style>
            .thumb {
                width: 405px;
                height: 100%;
                border: thin solid #dedede;
                margin: 0px 15px 0px 0px;
            }

            .thumb.current {
                border: 1px solid #555;
            }

            .thumb .close {
                position: relative;
                float: right;
                font-weight: bold;
                font-size: 20px;
                padding: 1px;
                cursor: pointer;
            }

            desabilitado{
                pointer-events: none;
            }

            .thumb .info {
                text-align: center;
                font-weight: bold;
                background: white;
                border: none;
                padding: 5px 0;
            }        

            #thumbnail img {
                width: 403px;
                height: 100px;
            }

            .action {
                border: none;
                padding: 5px;
                margin: 5px;
                color: white;
            }
        </style>
    </head>
    <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
            <div class="wrapper">
            <jsp:include page="../utils/menu.jsp">
                <jsp:param name="ativo" value="liPergunta"/>
                <jsp:param name="ativo1" value="liPergunta1"/>
            </jsp:include>
            <div style="margin: 15px">
                <!-- ******CONTENT****** --> 
                <div class="page-wrapper" >
                    <div class="page-wrapper" >
                        <header class="page-heading clearfix" style="margin-top: -20px">
                            <h1 class="heading-title pull-left">Formul�rio de Edi��o de Modalidade</h1>
                        </header>
                        <div class="page-content">
                            <div class="row page-row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <c:url var="post_url" value="/user/update"/>
                                    <form:form enctype="multipart/form-data" commandName="usuario" class="form-horizontal" method="POST" action="${post_url}">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados do Usu�rio</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                                        <label class="control-label" for="codLogin">C�digo</label>
                                                        <input type="text" readonly="true"  class="form-control" name="codLogin" id="codLogin" value="${codigo}" />
                                                    </div>
                                                    <div class="col-sm-10 col-md-10 col-lg-10">
                                                        <label class="control-label" for="usuario">Usu�rio</label>
                                                        <form:input path="usuario" readonly="true" type="text" required="true" onblur="if($('#respostaUserServer').html() === 'Usu�rio indisponivel para cadastro!'){ $(this).val('');$('#respostaUserServer').html('') }"  class="form-control" name="usuario" id="username" />
                                                        <p id="respostaUserServer" style="margin: 0;padding: 0;font-size: 14px;"></p>
                                                        <form:errors path="usuario" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                                        <label class="control-label" for="passwd">Senha</label>
                                                        <form:input path="passwd" required="true" readonly="true" type="password"  class="form-control" name="passwd" id="passwd" />
                                                        <form:errors path="passwd" />
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                                        <label class="control-label" for="passwd">Confirmar Senha</label>
                                                        <input required="true" type="password" class="form-control" readonly="true" onblur="verificaPasswd()" name="confirmarPasswd" id="confirmarPasswd" />
                                                        <form:errors path="passwd" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                                                <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Regras</h2>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                                        <label class="control-label" for="cep">Funcion�rio Respons�vel</label>   
                                                        <form:select path="funcionario.codFuncionario" required="true" readonly="true" id="funcionario" name="funcionario.codFuncionario" class="form-control">
                                                            <form:options items="${funcionarios}" itemValue="codFuncionario" itemLabel="nomeCompleto"/>
                                                        </form:select> 
                                                        <form:errors path="funcionario.codFuncionario" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                                        <label class="control-label" for="cep">Nivel Administrativo</label>   
                                                        <form:select path="roles" multiple="true" readonly="true" required="true"
                                                                     id="roles" name="roles" class="form-control">
                                                            <form:options items="${regras}" itemValue="nome" itemLabel="nome"/>
                                                        </form:select> 
                                                        <form:errors path="roles" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="pull-right" style="margin-right: 15px">
                                                        <a class="btn btn-default" type="button" href="<c:url value="/home" />">Voltar</a>
                                                        <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Cadastrar">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form:form>
                                </div>
                            </div>
                        </div><!--//page-content-->
                    </div><!--//page--> 
                </div>
            </div>
            <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
            <div id="modal"></div>
        </div><!--//wrapper-->
        <jsp:include page="../utils/rodape.jsp"></jsp:include>
            <!-- Javascript -->
            <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-1.12.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/plugins/bootstrap-hover-dropdown.min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/back-to-top.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jquery-placeholder/jquery.placeholder.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/flexslider/jquery.flexslider-min.js'/>" ></script>
        <script type="text/javascript" src="<c:url value='/assets/plugins/jflickrfeed/jflickrfeed.min.js'/>" ></script> 
        <!--MASCARAS e CEP-->
        <script type="text/javascript" src="<c:url value='/assets/js/jquery.maskedinput.js'/>" ></script>
        <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
        <!---->
        <!-- cookie -->
        <script src="<c:url value='/assets/js/jquery.cookie.js'/>"></script>
        <!---->
        <script type="text/javascript" src="<c:url value='/assets/js/main.js'/>"></script> 
        <script type="text/javascript" src="<c:url value='/assets/js/jquery-ui.js'/>"></script> 
        <!---->
        <!--datatable-->
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/assets/js/datatable/page-data-tables.js" />"></script> 
        <!---->
        <!--MENU-->
        <script src="<c:url value="/assets/js/js-Menu.js" />" type="text/javascript" charset="utf-8"></script>
        <!---->
        <script>
                                                            function editar(b)
                                                            {
                                                                $('#nomeModalidade').removeAttr("readonly");
                                                                $('#codTipoInstituicao').removeAttr("readonly");

                                                                event.preventDefault();
                                                                $(b).attr("onclick", "");
                                                                $(b).attr("type", "submit");
                                                                $(b).attr("value", "Atualizar");
                                                            }

                                                            function modal()
                                                            {
                                                                var titulo = 'Atualizar Modaliadade',
                                                                        texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br /> Modalidade atualizada com �xito!',
                                                                        funcao = '';
                                                                $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
                                                                $('#btn_modal').trigger('click');
                                                            }
        </script>
    </body>
</html>
