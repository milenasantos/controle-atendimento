<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Sistema de Processo Web</title>
    <!-- Meta -->
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="assets/images/logo_1.png">  
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
    <!-- Global CSS -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
    <!-- Plugins CSS -->    
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
    <link rel="stylesheet" href="assets/plugins/pretty-photo/css/prettyPhoto.css">    
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
    <!--datatable-->
    <link rel="stylesheet" href="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.css">
    <!---->
  </head>
  <body style="background: #fffffd" <c:if test="${success != null}">onload="modal()"</c:if>>
    <div class="wrapper">
      <jsp:include page="../utils/menu.jsp">
        <jsp:param name="ativo" value="liUsuario"/>
        <jsp:param name="ativo1" value="liUsuario1"/>
      </jsp:include>
      <div style="margin: 15px">
        <!-- ******CONTENT****** --> 
          <div class="page-wrapper" >
            <header class="page-heading clearfix" style="margin-top: -20px">
            <h1 class="heading-title pull-left">Formul�rio de Cadastro do Usu�rio</h1>
          </header>
            <div class="page-content">
              <div class="row page-row">
                <c:url var="post_url" value="/user"/>
                <form:form  commandName="usuario" class="form-horizontal" method="POST" action="${post_url}">
                  <form:input path="situacao" name="situacao" type="hidden" value="true" />
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                      <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Dados do Usu�rio</h2>
                    </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <div class="col-sm-2 col-md-2 col-lg-2">
                          <label class="control-label" for="codLogin">C�digo</label>
                          <input type="text" readonly="true"  class="form-control" name="codLogin" id="codLogin" value="${codigo}" />
                        </div>
                        <div class="col-sm-10 col-md-10 col-lg-10">
                          <label class="control-label" for="usuario">Usu�rio</label>
                          <form:input path="usuario" type="text" required="true" onblur="if($('#respostaUserServer').html() === 'Usu�rio indisponivel para cadastro!'){ $(this).val('');$('#respostaUserServer').html('') }"  class="form-control" name="usuario" id="username" />
                          <p id="respostaUserServer" style="margin: 0;padding: 0;font-size: 14px;"></p>
                          <form:errors path="usuario" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                          <label class="control-label" for="passwd">Senha</label>
                          <form:input path="passwd" required="true" type="password"  class="form-control" name="passwd" id="passwd" />
                          <form:errors path="passwd" />
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                          <label class="control-label" for="passwd">Confirmar Senha</label>
                          <input required="true" type="password" class="form-control" onblur="verificaPasswd()" name="confirmarPasswd" id="confirmarPasswd" />
                          <form:errors path="passwd" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center; border-bottom: 1px solid #dadada !important">
                      <h2 style="margin-top: -5px; margin-bottom: -5px;font-family: 'Open Sans', sans-serif; font-weight: 300;">Regras</h2>
                    </div>
                    <div class="panel-body">
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label class="control-label" for="cep">Funcion�rio Respons�vel</label>   
                          <form:select path="funcionario.codFuncionario" required="true"  id="funcionario" name="funcionario.codFuncionario" class="form-control">
                            <form:options items="${funcionarios}" itemValue="codFuncionario" itemLabel="nomeCompleto"/>
                          </form:select> 
                          <form:errors path="funcionario.codFuncionario" />
                        </div>
                        <div class="col-md-6 col-sm-6 col-lg-6">
                          <label class="control-label" for="cep">Nivel Administrativo</label>   
                          <form:select path="roles" multiple="true" required="true"
                                       id="roles" name="roles" class="form-control">
                            <form:options items="${regras}" itemValue="nome" itemLabel="nome"/>
                          </form:select> 
                          <form:errors path="roles" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="pull-right" style="margin-right: 15px">
                          <a class="btn btn-default" type="button" href="<c:url value="/home" />">Voltar</a>
                          <input id="btnFinalizar" name="btnFinalizar" class="btn btn-primary" type="submit" value="Cadastrar">
                        </div>
                      </div>
                    </div>
                  </div>
                </form:form>
              </div>
            </div><!--//page-content-->
          </div><!--//page--> 
      </div>
      <button type="button" class="btn btn-info btn-lg" id="btn_modal" data-toggle="modal" style="display: none" data-target="#myModal"></button>
      <div id="modal"></div>
    </div><!--//wrapper-->
    <jsp:include page="../utils/rodape.jsp"></jsp:include>
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
    <!--MASCARAS e CEP-->
    <script type="text/javascript" src="assets/js/jquery.maskedinput.js"></script>
    <script src="<c:url	value='/assets/js/CEP-Mask.js'/>"></script>
    <!---->
    <!-- cookie -->
    <script src="assets/js/jquery.cookie.js"></script>
    <!---->
    <script type="text/javascript" src="assets/js/main.js"></script> 
    <!---->
    <!--datatable-->
    <script type="text/javascript" src="assets/js/datatable/jquery.datatables/js/jquery.dataTables.min.js"></script> 
    <script type="text/javascript" src="assets/js/datatable/jquery.datatables/plugins/bootstrap/3/dataTables.bootstrap.js"></script> 
    <script type="text/javascript" src="assets/js/datatable/page-data-tables.js"></script> 
    <!---->
    <!--MENU-->
    <script src="assets/js/js-Menu.js" type="text/javascript" charset="utf-8"></script>
    <!---->
    <script>
    $(document).ready(function ()
    {
      $('#username').keyup(function()
      {
        var conteudo = $(this).val();
        var url = "<c:url value="/user/verifyUser/" />"+conteudo+".json";
        if(conteudo.length >= 4)
        {
          $.ajax(
          {
            type: "GET",
            datatype: "json",
            url: url,
            success: function(r)
            {
              if(r["tamanho"] === true)
              {
                if(r["livre"] === true)
                {
                  $('#respostaUserServer').html("Usu�rio livre para cadastro!");
                  $('#respostaUserServer').css("color","#15ed2e");
                }
                else
                {
                  $('#respostaUserServer').html("Usu�rio indisponivel para cadastro!");
                  $('#respostaUserServer').css("color","#f9161a");
                }
              }
              else
              {
                $('#respostaUserServer').html("O Usu�rio deve conter mais de 4 caracteres!");
                $('#respostaUserServer').css("color","#f9161a");
              }
            },
            error: function( error )
            {
              console.log(error);
            }
          });
        }
        else
        {
          $('#respostaUserServer').html("O Usu�rio deve conter mais de 4 caracteres!");
          $('#respostaUserServer').css("color","#f9161a");
        }
      });
        
    });

    function verificaPasswd()
    {
      if ($('#passwd').val() !== "" && $('#confirmarPasswd').val() !== "")
      {
        if ($('#passwd').val() !== $('#confirmarPasswd').val())
        {
          alert("Os campos de senha n�o conferem");
          $('#confirmarPasswd').val('');
        }
      }
    }
    
    function modal()
    {
        var titulo = 'Inserir Usu�rio',
            texto = '<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-floppy-o fa-stack-1x fa-inverse"></i> </span>  <strong><br />Sucesso! </strong><br /> Usu�rio cadastrado com �xito!',
            funcao = '';
        $('#modal').html('<div class="modal fade" style="margin-top: 75px" id="myModal" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title" style="text-align:center">' + titulo + '</h4></div><div class="modal-body col-sm-12"><div class="alert alert-question" style="text-align:center;font-size:15px">' + texto + '</div><div class="col-sm-12"><h4 id="textoAbaixo" style="text-align:justify"></h4></div></div><div class="modal-footer"><button type="button" class="btn btn-primary" data-dismiss="modal" onclick="' + funcao + '">Prosseguir</button></div></div></div></div>');
        $('#btn_modal').trigger('click');
    }

    </script>
  </body>
</html> 
