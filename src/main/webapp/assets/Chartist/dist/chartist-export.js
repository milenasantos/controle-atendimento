function inlineCSStoSVG(id) {
  var nodes = document.querySelectorAll("#" + id + " *");
  for (var i = 0; i < nodes.length; ++i) {
    var elemCSS = window.getComputedStyle(nodes[i], null);
    nodes[i].removeAttribute('xmlns');
    nodes[i].style.fill = elemCSS.fill;
    nodes[i].style.fillOpacity = elemCSS.fillOpacity;
    nodes[i].style.stroke = elemCSS.stroke;
    nodes[i].style.strokeLinecap = elemCSS.strokeLinecap;
    nodes[i].style.strokeDasharray = elemCSS.strokeDasharray;
    nodes[i].style.strokeWidth = elemCSS.strokeWidth;
    nodes[i].style.fontSize = elemCSS.fontSize;
    nodes[i].style.fontFamily = elemCSS.fontFamily;
    if (nodes[i].nodeName === "SPAN") {
      nodes[i].setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
    }
  }
}

function exportToCanvas(id) {

  var svgElem = document.querySelector("#" + id + " svg");
  svgElem.setAttribute("xmlns", "http://www.w3.org/2000/svg");

  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  ctx.canvas.height = svgElem.clientHeight;
  ctx.canvas.width = svgElem.clientWidth;

  var DOMURL = window.URL || window.webkitURL || window;
  var img = new Image();
  img.crossOrigin = "Anonymous";
  var blob = undefined;
  try {
    blob = new Blob([svgElem.outerHTML], {
      type: "image/svg+xml;charset=utf-8"
    });
  }
  catch (e) {
    if (e.name == "InvalidStateError") {
      var bb = new MSBlobBuilder();
      bb.append(svgElem.outerHTML);
      blob = bb.getBlob("image/svg+xml;charset=utf-8");
    }
    else {
      throw e;
    }
  }
  var url = DOMURL.createObjectURL(blob);
  img.onload = function() {
    ctx.drawImage(img, 0, 0);
    DOMURL.revokeObjectURL(url);
  }
  img.src = url;
  
  
  if (window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, "Chart");
  } else {
    const a = document.createElement('a');
    document.body.appendChild(a);
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = "Chart";
    a.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    }, 0);
  }


  
}

